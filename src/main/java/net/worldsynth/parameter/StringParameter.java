/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.standalone.ui.parameters.StringParameterField;
import net.worldsynth.synth.io.Element;

public class StringParameter extends AbstractParameter<String> {
	
	public StringParameter(String name, String displayName, String description, String defaultValue) {
		super(name, displayName, description, defaultValue);
	}
	
	@Override
	public ParameterUiElement<String> getUi() {
		return new StringParameterField(this);
	}
	
	@Override
	public Element toElement() {
		return new Element(name, getValue());
	}

	@Override
	public void fromElement(Element element) {
		setValue(element.content);
	}
}
