/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.standalone.ui.parameters.table.TableParameterTable;
import net.worldsynth.synth.io.Element;

public abstract class TableParameter<P> extends AbstractParameter<List<P>> {
	
	private List<TableColumn<P, ?>> columns;
	
	@SafeVarargs
	public TableParameter(String name, String displayName, String description, List<P> defaultValue, TableColumn<P, ?> ... columns) {
		super(name, displayName, description, defaultValue);
		this.columns = Arrays.asList(columns);
	}

	@Override
	public ParameterUiElement<List<P>> getUi() {
		return new TableParameterTable<P>(this, columns) {
		};
	}
	
	public abstract P newRow();

	@Override
	public Element toElement() {
		ArrayList<Element> rowElements = new ArrayList<Element>();
		for(P row: getValue()) {
			rowElements.add(rowToElement(row));
		}
		return new Element(name, rowElements);
	}
	
	private Element rowToElement(P row) {
		ArrayList<Element> rowDataElements = new ArrayList<Element>();
		for(TableColumn<P, ?> column: columns) {
			rowDataElements.add(column.columnValueToElement(row));
		}
		return new Element("row", rowDataElements);
	}

	@Override
	public void fromElement(Element element) {
		ArrayList<P> rows = new ArrayList<P>();
		for(Element rowElement: element.elements) {
			if(rowElement.tag.equals("row")) {
				rows.add(rowFromElement(rowElement));
			}
		}
		setValue(rows);
	}
	
	private P rowFromElement(Element element) {
		P row = newRow();
		for(TableColumn<P, ?> column: columns) {
			column.setColumnValueFromElement(row, element.getElement(column.getName()));
		}
		return row;
	}
}
