/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.table;

import javafx.scene.control.Control;
import net.worldsynth.synth.io.Element;

public abstract class TableColumn<P, V> {
	
	protected final String name;
	protected final String displayName;
	protected final String description;
	
	public TableColumn(String name, String displayName, String description) {
		this.name = name;
		this.displayName = displayName;
		this.description = description;
	}
	
	public final String getName() {
		return name;
	}
	
	public final String getDisplayName() {
		return displayName;
	}
	
	public final String getDescription() {
		return description;
	}
	
	public final Control getNewUiControlFromRow(P row, OnChange notifier) {
		return getNewUiControl(getColumnValue(row), notifier);
	}
	
	public abstract V getColumnValue(P row);
	public abstract void setColumnValue(P row, V value);
	
	protected abstract Control getNewUiControl(V value, OnChange notifier);
	public abstract V getColumnValueFromUiControl(Control control);
	public final void setColumnValueFromUiControl(P row, Control control) {
		setColumnValue(row, getColumnValueFromUiControl(control));
	}
	
	public abstract V getColumnValueFromElement(Element element);
	public final void setColumnValueFromElement(P row, Element element) {
		setColumnValue(row, getColumnValueFromElement(element));
	}
	public abstract Element columnValueToElement(P row);
}
