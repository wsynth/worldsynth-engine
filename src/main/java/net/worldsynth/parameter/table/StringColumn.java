/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.table;

import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import net.worldsynth.synth.io.Element;

public abstract class StringColumn<P> extends TableColumn<P, String> {

	public StringColumn(String name, String displayName, String description) {
		super(name, displayName, description);
	}

	@Override
	public Control getNewUiControl(String value, OnChange notifier) {
		TextField textField = new TextField(value);
		textField.textProperty().addListener((observable, oldValue, newValue) -> {
			notifier.call();
		});
		return textField;
	}
	
	@Override
	public String getColumnValueFromUiControl(Control control) {
		TextField textField = (TextField) control;
		return textField.getText();
	}
	
	@Override
	public String getColumnValueFromElement(Element element) {
		return element.content;
	}
	
	@Override
	public Element columnValueToElement(P row) {
		String string = getColumnValue(row);
		return new Element(name, string);
	}
}
