/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import net.worldsynth.biome.Biome;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.standalone.ui.parameters.BiomeParameterSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.io.Element;

public class BiomeParameter extends AbstractParameter<Biome> {
	
	public BiomeParameter(String name, String displayName, String description, Biome defaultValue) {
		super(name, displayName, description, defaultValue);
	}
	
	@Override
	public ParameterUiElement<Biome> getUi() {
		return new BiomeParameterSelector(this);
	}
	
	@Override
	public Element toElement() {
		return new Element(name, getValue().getIdName());
	}

	@Override
	public void fromElement(Element element) {
		setValue(BiomeRegistry.getBiome(element.content));
	}
}
