/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import net.worldsynth.standalone.ui.parameters.BooleanParameterCheckbox;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.io.Element;

public class BooleanParameter extends AbstractParameter<Boolean> {
	
	public BooleanParameter(String name, String displayName, String description, boolean defaultValue) {
		super(name, displayName, description, defaultValue);
	}
	
	@Override
	public ParameterUiElement<Boolean> getUi() {
		return new BooleanParameterCheckbox(this);
	}
	
	@Override
	public Element toElement() {
		return new Element(name, String.valueOf(getValue()));
	}

	@Override
	public void fromElement(Element element) {
		setValue(Boolean.parseBoolean(element.content));
	}
}
