/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import net.worldsynth.standalone.ui.parameters.ObjectParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.io.Element;

public class ObjectParameter extends AbstractParameter<Object> {
	
	private Object[] selectables;
	
	public ObjectParameter(String name, String displayName, String description, Object defaultValue, Object... selectables) {
		super(name, displayName, description, defaultValue);
		
		this.selectables = selectables;
	}
	
	@Override
	public ParameterUiElement<Object> getUi() {
		return new ObjectParameterDropdownSelector(this);
	}
	
	@Override
	public Element toElement() {
		return new Element(name, getValue().getClass().toString());
	}

	@Override
	public void fromElement(Element element) {
		for(Object selectable: selectables) {
			if(element.content.equals(selectable.getClass().toString())) {
				setValue(selectable);
				return;
			}
		}
	}
	
	public Object[] getSelectables() {
		return selectables;
	}
}
