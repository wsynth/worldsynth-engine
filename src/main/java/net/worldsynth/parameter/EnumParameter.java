/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.io.Element;

public class EnumParameter<E extends Enum<E>> extends AbstractParameter<E> {
	
	protected Class<E> enumClass;
	
	public EnumParameter(String name, String displayName, String description, Class<E> enumClass, E defaultValue) {
		super(name, displayName, description, defaultValue);
		
		this.enumClass = enumClass;
	}
	
	@Override
	public ParameterUiElement<E> getUi() {
		return new EnumParameterDropdownSelector<E>(this);
	}
	
	@Override
	public Element toElement() {
		return new Element(name, getValue().name());
	}

	@Override
	public void fromElement(Element element) {
		for(E type: enumClass.getEnumConstants()) {
			if(element.content.equals(type.name())) {
				setValue(type);
				return;
			}
		}
	}
	
	public Class<E> getEnumClass() {
		return enumClass;
	}
}
