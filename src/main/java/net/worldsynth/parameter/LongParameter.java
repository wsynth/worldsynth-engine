/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import net.worldsynth.standalone.ui.parameters.LongParameterField;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.io.Element;

public class LongParameter extends AbstractParameter<Long> {
	
	protected long min;
	protected long max;
	
	protected long sliderMin;
	protected long sliderMax;
	
	public LongParameter(String name, String displayName, String description, long defaultValue, long min, long max) {
		this(name, displayName, description, defaultValue, min, max, min, max);
	}
	
	public LongParameter(String name, String displayName, String description, long defaultValue, long min, long max, long sliderMin, long sliderMax) {
		super(name, displayName, description, defaultValue);
		this.min = min;
		this.max = max;
		this.sliderMin = sliderMin;
		this.sliderMax = sliderMax;
	}
	
	@Override
	public ParameterUiElement<Long> getUi() {
		return new LongParameterField(this);
	}
	
	@Override
	public Element toElement() {
		return new Element(name, String.valueOf(getValue()));
	}

	@Override
	public void fromElement(Element element) {
		setValue(Long.parseLong(element.content));
	}
	
	public long getMin() {
		return min;
	}
	
	public long getMax() {
		return max;
	}
	
	public long getSliderMin() {
		return sliderMin;
	}
	
	public long getSliderMax() {
		return sliderMax;
	}
}
