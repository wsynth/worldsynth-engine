/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import javafx.event.EventHandler;
import net.worldsynth.extent.ExtentEvent;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.io.Element;

public class WorldExtentParameter extends AbstractParameter<WorldExtent> {

	private final AbstractModule parentModule;

	private EventHandler<ExtentEvent> extentHandler;
	
	public WorldExtentParameter(String name, String displayName, String description, AbstractModule parentModule) {
		super(name, displayName, description, null);
		this.parentModule = parentModule;
		
		extentHandler = e -> {
			if(e.getEventType() == ExtentEvent.EXTENT_REMOVED) {
				setExtent(null);
			}
		};
	}
	
	private WorldExtentManager getExtentManager() {
		return parentModule.getExtentManager();
	}

	public void setExtent(WorldExtent extent) {
		if(getValue() != null) {
			getValue().removeEventHandler(ExtentEvent.ANY, extentHandler);
		}

		setValue(extent);

		if(extent != null) {
			extent.addEventHandler(ExtentEvent.ANY, extentHandler);
		}
	}

	public void setExtentAsString(String extentString) {
		setValue(null);

		if(getExtentManager() != null && extentString != null) {
			String s = extentString.substring(extentString.indexOf("#") + 1);
			long id = Long.parseLong(s);
			setExtent(getExtentManager().getWorldExtentById(id));
		}
	}

	public String getExtentAsString() {
		if(getValue() != null) {
			return getValue().getName() + "#" + getValue().getId();
		}
		return null;
	}

	@Override
	public ParameterUiElement<WorldExtent> getUi() {
		return new ExtentParameterDropdownSelector(this, getExtentManager());
	}

	@Override
	public Element toElement() {
		return new Element(name, getExtentAsString());
	}

	@Override
	public void fromElement(Element element) {
		setExtentAsString(element.content);
	}
}
