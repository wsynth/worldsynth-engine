/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.standalone.ui.parameters.MaterialParameterSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.io.Element;

public class MaterialParameter extends AbstractParameter<Material<?, ?>> {
	
	public MaterialParameter(String name, String displayName, String description, Material<?, ?> defaultValue) {
		super(name, displayName, description, defaultValue);
	}
	
	@Override
	public ParameterUiElement<Material<?, ?>> getUi() {
		return new MaterialParameterSelector(this);
	}
	
	@Override
	public Element toElement() {
		return new Element(name, getValue().getIdName());
	}

	@Override
	public void fromElement(Element element) {
		setValue(MaterialRegistry.getMaterial(element.content));
	}
}
