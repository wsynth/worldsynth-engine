/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import java.io.File;

import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.standalone.ui.parameters.FileParameterField;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.io.Element;

public class FileParameter extends AbstractParameter<File> {

	protected boolean save;
	protected boolean directory;
	protected ExtensionFilter fileExtensions;
	
	public FileParameter(String name, String displayName, String description, File defaultValue, boolean save, boolean directory, ExtensionFilter fileExtensions) {
		super(name, displayName, description, defaultValue);
		
		this.save = save;
		this.directory = directory;
		this.fileExtensions = fileExtensions;
	}
	
	@Override
	public ParameterUiElement<File> getUi() {
		return new FileParameterField(this);
	}
	
	@Override
	public Element toElement() {
		if(getValue() == null) return new Element(name, "");
		return new Element(name, getValue().getPath());
	}

	@Override
	public void fromElement(Element element) {
		setValue(new File(element.content));
	}
	
	public boolean isSave() {
		return save;
	}
	
	public boolean isDirectory() {
		return directory;
	}
	
	public ExtensionFilter getFilter() {
		return fileExtensions;
	}
}
