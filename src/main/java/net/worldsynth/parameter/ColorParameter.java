/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import java.util.ArrayList;

import net.worldsynth.common.color.WsColor;
import net.worldsynth.standalone.ui.parameters.ColorParameterSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.io.Element;

public class ColorParameter extends AbstractParameter<WsColor> {

	public ColorParameter(String name, String displayName, String description, WsColor defaultValue) {
		super(name, displayName, description, defaultValue);
	}

	@Override
	public ParameterUiElement<WsColor> getUi() {
		return new ColorParameterSelector(this);
	}

	@Override
	public Element toElement() {
		ArrayList<Element> colorRgbComponentElements = new ArrayList<Element>();
		colorRgbComponentElements.add(new Element("red", String.valueOf(getValue().getRed())));
		colorRgbComponentElements.add(new Element("green", String.valueOf(getValue().getGreen())));
		colorRgbComponentElements.add(new Element("blue", String.valueOf(getValue().getBlue())));
		if(getValue().getOpacity() < 1.0f) {
			colorRgbComponentElements.add(new Element("opacity", String.valueOf(getValue().getOpacity())));
		}
		return new Element(name, colorRgbComponentElements);
	}

	@Override
	public void fromElement(Element element) {
		float red = Float.parseFloat(element.getElement("red").content);
		float green = Float.parseFloat(element.getElement("green").content);
		float blue = Float.parseFloat(element.getElement("blue").content);
		float opacity = 1.0f;
		Element opacityElement = element.getElement("opacity");
		if(opacityElement != null) {
			opacity = Float.parseFloat(opacityElement.content);
		}
		setValue(new WsColor(red, green, blue, opacity));
	}

}
