/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import org.controlsfx.control.PrefixSelectionComboBox;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import net.worldsynth.biome.Biome;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.parameter.BiomeParameter;

public class BiomeParameterSelector extends ParameterUiElement<Biome> {
	
	private Label nameLabel;
	private PrefixSelectionComboBox<Biome> biomeDropdownSelector;
	
	public BiomeParameterSelector(BiomeParameter parameter) {
		super(parameter);
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if(parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		//Dropdown selector
		biomeDropdownSelector = new PrefixSelectionComboBox<Biome>();
//		materialDropdownSelector.setTypingDelay(1000);
		for(Biome biome: BiomeRegistry.getBiomesAlphabetically()) {
			biomeDropdownSelector.getItems().add(biome);
		}
		biomeDropdownSelector.getSelectionModel().select(uiValue);
		biomeDropdownSelector.valueProperty().addListener(new ChangeListener<Biome>() {

			@Override
			public void changed(ObservableValue<? extends Biome> observable, Biome oldValue, Biome newValue) {
				uiValue = newValue;
				notifyChangeHandlers();
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		biomeDropdownSelector.setDisable(disable);
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(biomeDropdownSelector, 1, row);
	}
}
