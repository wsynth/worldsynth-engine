/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.GridPane;
import net.worldsynth.parameter.table.TableColumn;
import net.worldsynth.parameter.table.TableParameter;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class TableParameterTable<P> extends ParameterUiElement<List<P>> {
	
	private final TableParameter<P> tableParameter;
	
	private ObservableList<RowUi> rowUiList = FXCollections.observableList(new ArrayList<RowUi>());
	
	private GridPane listLayout;
	private ScrollPane listScrolPane;
	
	private Button addButton;
	
	private int lastRemovedListIndex;

	public TableParameterTable(TableParameter<P> parameter, List<TableColumn<P, ?>> collumns) {
		super(parameter);
		tableParameter = parameter;

		uiValue = new ArrayList<P>(parameter.getValue());
		
		for(P p: uiValue) {
			rowUiList.add(new RowUi(p, collumns));
		}
		
		rowUiList.addListener(new ListChangeListener<RowUi>() {

			@Override
			public void onChanged(Change<? extends RowUi> c) {
				updateListUi();
				updateUiValue();
			}
			
		});
		
		listLayout = new GridPane();
		listScrolPane = new ScrollPane(listLayout);
		listScrolPane.setPrefHeight(300);
		listScrolPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		
		addButton = new Button("Add row");
		addButton.setOnAction(e -> {
			rowUiList.add(new RowUi(parameter.newRow(), collumns));
		});
		
		//Build gridLayout
		updateListUi();
	}
	
	private void updateListUi() {
		listLayout.getChildren().clear();
		
		int i = 0;
		for(RowUi e: rowUiList) {
			int j = 0;
			listLayout.add(new Label(String.valueOf(i)), j++, i);
			
			for(Control c: e.columnControlMap.values()) {
				listLayout.add(c, j++, i);
			}
		
			listLayout.add(e.removeButton, j++, i);
			
			if(i++ == lastRemovedListIndex) {
				e.removeButton.requestFocus();
			}
		}
		
		listLayout.add(addButton, 0, i, 3, 1);
		if(i == lastRemovedListIndex) {
			addButton.requestFocus();
		}
		
		lastRemovedListIndex = -1;
	}
	
	private void updateUiValue() {
		uiValue = new ArrayList<P>();
		for(RowUi e: rowUiList) {
			uiValue.add(e.getRowValue());
		}
		
		notifyChangeHandlers();
	}

	@Override
	public void setDisable(boolean disable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(listScrolPane, 0, row, 3, 1);
	}
	
	private class RowUi {
		HashMap<TableColumn<P, ?>, Control> columnControlMap;
		Button removeButton;
		
		public RowUi(P row, List<TableColumn<P, ?>> columns) {
			columnControlMap = new HashMap<TableColumn<P, ?>, Control>();
			for(TableColumn<P, ?> column: columns) {
				
				Control newControl = column.getNewUiControlFromRow(row, () -> {
					updateUiValue();
				});
				
				columnControlMap.put(column, newControl);
			}
			
			removeButton = new Button("Remove");
			removeButton.setOnAction(e -> {
				lastRemovedListIndex = rowUiList.indexOf(this);
				rowUiList.remove(this);
			});
		}
		
		public P getRowValue() {
			P row = tableParameter.newRow();
			for(Entry<TableColumn<P, ?>, Control> e: columnControlMap.entrySet()) {
				e.getKey().setColumnValueFromUiControl(row, e.getValue());
			}
			return row;
		}
	}
}
