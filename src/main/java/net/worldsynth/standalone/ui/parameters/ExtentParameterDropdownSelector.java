/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import net.worldsynth.extent.ExtentEvent;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.parameter.WorldExtentParameter;

public class ExtentParameterDropdownSelector extends ParameterUiElement<WorldExtent> {
	
	private Label nameLabel;
	private ComboBox<WorldExtent> parameterDropdownSelector;

	private boolean ignoreSelectionChange = false;
	
	public ExtentParameterDropdownSelector(WorldExtentParameter parameter, WorldExtentManager extentManager) {
		super(parameter);
		
		// FIXME The event handler is never removed (potential memory leak?)
		extentManager.addEventHandler(ExtentEvent.ANY, e -> {
			if(e.getEventType() == ExtentEvent.EXTENT_REMOVED && e.getExtent() == uiValue) {
				uiValue = null;
			}
			ignoreSelectionChange = true;
			setExtentDropdownItems(extentManager.getObservableExtentsList());
			parameterDropdownSelector.setValue(uiValue);
			ignoreSelectionChange = false;
		});
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if(parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		//Dropdown selector
		parameterDropdownSelector = new ComboBox<WorldExtent>();
		setExtentDropdownItems(extentManager.getObservableExtentsList());
		parameterDropdownSelector.getSelectionModel().select(uiValue);
		parameterDropdownSelector.valueProperty().addListener(new ChangeListener<WorldExtent>() {

			@Override
			public void changed(ObservableValue<? extends WorldExtent> observable, WorldExtent oldValue, WorldExtent newValue) {
				if(!ignoreSelectionChange) {
					uiValue = newValue;
					notifyChangeHandlers();
				}
			}
		});
	}

	private void setExtentDropdownItems(ObservableList<WorldExtent> items) {
		parameterDropdownSelector.getItems().clear();
		for(WorldExtent par: items) {
			parameterDropdownSelector.getItems().add(par);
		}
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		parameterDropdownSelector.setDisable(disable);
	}

	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(parameterDropdownSelector, 1, row);
	}
}
