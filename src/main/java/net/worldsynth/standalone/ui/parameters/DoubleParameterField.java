/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import net.worldsynth.parameter.DoubleParameter;

public class DoubleParameterField extends ParameterUiElement<Double> {
	
	private Label nameLabel;
	private TextField parameterField;
	
	public DoubleParameterField(DoubleParameter parameter) {
		super(parameter);
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if(parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		//Field
		parameterField = new TextField(String.valueOf(uiValue));
		parameterField.setPrefColumnCount(40);
		parameterField.setTooltip(new Tooltip("Range: " + parameter.getMin() + " - " + parameter.getMax()));
		parameterField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				String text = parameterField.getText().replace(",", ".");
				try {
					double v = Double.parseDouble(text);
					if(v < parameter.getMin() || v > parameter.getMax()) throw new Exception("Value is out of range");
					uiValue = v;
					parameterField.setStyle(null);
				} catch (Exception exception) {
					parameterField.setStyle("-fx-background-color: RED;");
				}
				notifyChangeHandlers();
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		parameterField.setDisable(disable);
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(parameterField, 1, row);
	}
}
