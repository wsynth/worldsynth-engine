/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.fx.control;

import javafx.scene.control.ColorPicker;
import javafx.scene.control.Skin;
import net.worldsynth.common.color.WsColor;
import net.worldsynth.fx.control.skin.WsColorPickerSkin;

public class WsColorPicker extends ColorPicker {
	
	public WsColorPicker(WsColor color) {
		super(color.getFxColor());
		getStyleClass().add("worldsynth-color-picker");
    }
	
	@Override
	protected Skin<?> createDefaultSkin() {
		return new WsColorPickerSkin(this);
	}
}
