/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.fx.control.skin;

import com.sun.javafx.scene.control.skin.ComboBoxPopupControl;

import javafx.scene.Node;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import net.worldsynth.fx.control.WsColorPicker;
import net.worldsynth.fx.control.behavior.WsColorPickerBehavior;

public class WsColorPickerSkin extends ComboBoxPopupControl<Color> {

	private WsDropdownColorPicker popupContent;

	public WsColorPickerSkin(final WsColorPicker colorPicker) {
		super(colorPicker, new WsColorPickerBehavior(colorPicker));
		registerChangeListener(colorPicker.valueProperty(), "VALUE");

		updateColor();
	}

	@Override
	protected double computePrefWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset) {
		return 100.0;
	}

	@Override
	protected Node getPopupContent() {
		if(popupContent == null) {
			popupContent = new WsDropdownColorPicker((WsColorPicker) getSkinnable());
		}
		return popupContent;
	}

	@Override
	protected void focusLost() {
		return;
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	protected void handleControlPropertyChanged(String p) {
		super.handleControlPropertyChanged(p);
		if(p.equals("VALUE")) {
			updateColor();
		}
	}

	@Override
	public Node getDisplayNode() {
		return null;
	}

	private void updateColor() {
		getSkinnable().setStyle("-fx-color: " + getFormatHexString() + ";"
				+ "-fx-body-color: " + getFormatHexString() + ";");
	}

	private String getFormatHexString() {
		Color c = ((ColorPicker) getSkinnable()).getValue();
		if(c != null) {
			return String.format("#%02x%02x%02x", Math.round(c.getRed() * 255), Math.round(c.getGreen() * 255), Math.round(c.getBlue() * 255));
		}
		else {
			return null;
		}
	}

	public void syncWithAutoUpdate() {
		if(!getPopup().isShowing() && getSkinnable().isShowing()) {
			getSkinnable().hide();
		}
	}

	@Override
	protected StringConverter<Color> getConverter() {
		return null;
	}

	@Override
	protected TextField getEditor() {
		return null;
	}
}
