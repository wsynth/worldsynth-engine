/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.fx.control.skin;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import org.controlsfx.control.PrefixSelectionComboBox;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import net.worldsynth.fx.control.MaterialStateSelector;
import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.material.StateProperties;
import net.worldsynth.material.Property;

public class DropdownMaterialStateSelector extends GridPane {
    
	private final ObjectProperty<MaterialState<?, ?>> materialStateProperty;
	private PrefixSelectionComboBox<MaterialState<?, ?>> materialStateDropdownSelector;
	
	private final ObjectProperty<Material<?, ?>> materialProperty;
	private PrefixSelectionComboBox<Material<?, ?>> materialDropdownSelector;
	
	private final ColorTextureViewer colorTextureViewer;
	private final PropertiesEditor propertiesEditor;
	
	public DropdownMaterialStateSelector(MaterialStateSelector materialStateSelector) {
		if(materialStateSelector.getValue() != null) {
			materialProperty = new SimpleObjectProperty<Material<?, ?>>(materialStateSelector.getValue().getMaterial());
		}
		else {
			materialProperty = new SimpleObjectProperty<Material<?,?>>(Material.NULL);
		}
		materialStateProperty = new SimpleObjectProperty<MaterialState<?, ?>>(materialStateSelector.getValue());
		
		//Change listeners
		materialProperty.addListener((observable, oldValue, newValue) -> {
			materialStateProperty.setValue(newValue.getDefaultState());
			materialStateDropdownSelector.getItems().setAll(newValue.getStates());
		});
		
		materialStateProperty.addListener((observable, oldValue, newValue) -> {
			materialStateSelector.setValue(newValue);
		});
		
		///////////////////////
		// UI Initialisation //
		///////////////////////
		
		//Material dropdown selector
		materialDropdownSelector = new PrefixSelectionComboBox<Material<?, ?>>();
		materialDropdownSelector.getItems().addAll(MaterialRegistry.getMaterialsAlphabetically());
		materialDropdownSelector.valueProperty().bindBidirectional(materialProperty);
		
		//MaterialStates dropdown selector
		materialStateDropdownSelector = new PrefixSelectionComboBox<MaterialState<?, ?>>();
		materialStateDropdownSelector.getItems().addAll(materialProperty.getValue().getStates());
		materialStateDropdownSelector.valueProperty().bindBidirectional(materialStateProperty);
		
		//Material color and texture preview
		colorTextureViewer = new ColorTextureViewer(materialStateProperty);
		
		//Properties editor
		propertiesEditor = new PropertiesEditor(materialProperty, materialStateProperty);
		
		
		//Layout
		setVgap(5);
		setHgap(5);
		getStyleClass().add("dropdown-materialstate-selector");
		
		ColumnConstraints columnConstraintsMaterial = new ColumnConstraints(128);
		columnConstraintsMaterial.setHgrow(Priority.NEVER);
		ColumnConstraints columnConstraintsState = new ColumnConstraints();
		columnConstraintsState.setHgrow(Priority.ALWAYS);
		getColumnConstraints().setAll(columnConstraintsMaterial, columnConstraintsState);
		
		materialDropdownSelector.setMaxWidth(Double.MAX_VALUE);
		GridPane.setFillWidth(materialDropdownSelector, true);
		
		materialStateDropdownSelector.setMaxWidth(Double.MAX_VALUE);
		GridPane.setFillWidth(materialStateDropdownSelector, true);
		
		add(materialDropdownSelector, 0, 0);
		add(materialStateDropdownSelector, 1, 0);
		add(colorTextureViewer, 0, 1);
		
		
		ScrollPane propertiesEditorScrolPane = new ScrollPane(propertiesEditor);
		propertiesEditorScrolPane.setMaxHeight(128);
		propertiesEditorScrolPane.setFitToWidth(true);
		propertiesEditorScrolPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		add(propertiesEditorScrolPane, 1, 1);
	}
	
	@Override
	public String getUserAgentStylesheet() {
		return getClass().getResource("worldsynth-materialstate-selector-dropdown.css").toExternalForm();
	}
	
	private class ColorTextureViewer extends StackPane {
		
		private Rectangle colorView;
		private ImageView textureView;
		
		public ColorTextureViewer(ObjectProperty<MaterialState<?, ?>> materialSateProperty) {
			setMinHeight(128);
			setMaxHeight(128);
			setMinWidth(128);
			setMaxWidth(128);
			
			colorView = new Rectangle(128, 128);
			textureView = new ImageView();
			
			this.getChildren().addAll(textureView, colorView);
			
			// Set a clip to apply rounded borders
			Rectangle clip = new Rectangle(128, 128);
			clip.setArcWidth(6);
			clip.setArcHeight(6);
			this.setClip(clip);
			
			updateColorAndTexture(materialSateProperty.getValue());
			materialSateProperty.addListener((observable, oldValue, newValue) -> {
				updateColorAndTexture(newValue);
			});
		}
		
		private void updateColorAndTexture(MaterialState<?, ?> materialState) {
			if(materialState == null) return;
			
			textureView.setImage(materialState.getTextureImage());
			if(materialState.getTextureImage() == null) {
				colorView.setFill(materialState.getFxColor());
			}
			else {
				LinearGradient gradient = new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE,
						new Stop(0, materialState.getFxColor()),
						new Stop(0.25, materialState.getFxColor()),
						new Stop(0.25, Color.TRANSPARENT),
						new Stop(1, Color.TRANSPARENT));
				colorView.setFill(gradient);
			}
		}
	}
	
	private class PropertiesEditor extends GridPane {
		
		private Material<?, ?> curentMaterial;
		
		private LinkedHashMap<String, ComboBox<String>> propertiesList = new LinkedHashMap<String, ComboBox<String>>();
		
		boolean ignorePropertyChange = false;
		private ChangeListener<String> propertyChangeListener;
		
		public PropertiesEditor(ObjectProperty<Material<?, ?>> materialProperty, ObjectProperty<MaterialState<?, ?>> paterialStateProperty) {
			setPadding(new Insets(10));
			setVgap(5);
			setHgap(5);
			
			ColumnConstraints columnConstraintsPropName = new ColumnConstraints();
			columnConstraintsPropName.setHgrow(Priority.SOMETIMES);
			ColumnConstraints columnConstraintsPropVal = new ColumnConstraints();
			columnConstraintsPropVal.setHgrow(Priority.ALWAYS);
			getColumnConstraints().setAll(columnConstraintsPropName, columnConstraintsPropVal);
			
			propertyChangeListener = (observable, oldValue, newValue) -> {
				if(ignorePropertyChange) return;
				MaterialState<?, ?> newState = curentMaterial.getState(buildPropertiesKey(propertiesList));
				materialStateProperty.setValue(newState);
			};
			
			curentMaterial = materialProperty.getValue();
			makePropertiesList(curentMaterial);
			
			updatePropertiesList(materialStateProperty.getValue());
			
			materialProperty.addListener((observable, oldValue, newValue) -> {
				if(newValue == curentMaterial) return;
				curentMaterial = newValue;
				makePropertiesList(newValue);
			});
			
			materialStateProperty.addListener((observable, oldValue, newValue) -> {
				if(newValue.getMaterial() != curentMaterial) {
					curentMaterial = newValue.getMaterial();
					makePropertiesList(curentMaterial);
				}
				updatePropertiesList(newValue);
			});
		}
		
		private void makePropertiesList(Material<?, ?> material) {
			getChildren().clear();
			
			if(material == null || material.getProperties() == null || material.getProperties().isEmpty()) return;
			
			propertiesList = new LinkedHashMap<String, ComboBox<String>>();
			int i = 0;
			for(Entry<String, List<String>> property: material.getProperties().entrySet()) {
				Label properyLabel = new Label(property.getKey() + ":");
				properyLabel.setAlignment(Pos.CENTER_RIGHT);
				GridPane.setFillWidth(properyLabel, true);
				properyLabel.setMaxWidth(Double.MAX_VALUE);
				add(properyLabel, 0, i);
				
				PrefixSelectionComboBox<String> propertiesValueDropdownSelector = new PrefixSelectionComboBox<String>();
				propertiesValueDropdownSelector.getItems().addAll(property.getValue());
				propertiesValueDropdownSelector.valueProperty().addListener(propertyChangeListener);
				propertiesValueDropdownSelector.setMaxWidth(Double.MAX_VALUE);
				GridPane.setFillWidth(propertiesValueDropdownSelector, true);
				add(propertiesValueDropdownSelector, 1, i);
				
				propertiesList.put(property.getKey(), propertiesValueDropdownSelector);
				
				i++;
			}
		}
		
		private void updatePropertiesList(MaterialState<?, ?> state) {
			ignorePropertyChange = true;
			if(state == null || state.getProperties() == null || state.getProperties().isEmpty() || propertiesList.isEmpty()) return;
			for(Property property: state.getProperties().asList()) {
				propertiesList.get(property.getName()).getSelectionModel().select(property.getValue());
			}
			ignorePropertyChange = false;
		}
		
		private StateProperties buildPropertiesKey(LinkedHashMap<String, ComboBox<String>> propertiesMap) {
			ArrayList<Property> propertiesList = new ArrayList<Property>();
			for(Entry<String, ComboBox<String>> prop: propertiesMap.entrySet()) {
				propertiesList.add(new Property(prop.getKey(), prop.getValue().getValue()));
			}
			return new StateProperties(propertiesList);
		}
	}
}
