/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.fx.control;

import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.Skin;
import net.worldsynth.fx.control.skin.MaterialStateSelectorSkin;
import net.worldsynth.material.MaterialState;

public class MaterialStateSelector extends ComboBoxBase<MaterialState<?, ?>> {
	
	public MaterialStateSelector(MaterialState<?, ?> state) {
		setValue(state);
		getStyleClass().add("worldsynth-materialstate-selector");
    }
	
	@Override
	protected Skin<?> createDefaultSkin() {
		return new MaterialStateSelectorSkin(this);
	}
}
