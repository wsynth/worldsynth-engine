/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.fx.control.behavior;

import static javafx.scene.input.KeyCode.ENTER;
import static javafx.scene.input.KeyCode.ESCAPE;
import static javafx.scene.input.KeyCode.SPACE;
import static javafx.scene.input.KeyEvent.KEY_PRESSED;

import java.util.ArrayList;
import java.util.List;

import com.sun.javafx.scene.control.behavior.ComboBoxBaseBehavior;
import com.sun.javafx.scene.control.behavior.KeyBinding;

import net.worldsynth.fx.control.MaterialStateSelector;
import net.worldsynth.fx.control.skin.MaterialStateSelectorSkin;
import net.worldsynth.material.MaterialState;

public class MaterialStateSelectorBehavior extends ComboBoxBaseBehavior<MaterialState<?, ?>> {

	public MaterialStateSelectorBehavior(final MaterialStateSelector colorPicker) {
		super(colorPicker, MATERIALSTATE_SELECTOR_BINDINGS);
	}
	
	protected static final List<KeyBinding> MATERIALSTATE_SELECTOR_BINDINGS = new ArrayList<KeyBinding>();
	static {
		MATERIALSTATE_SELECTOR_BINDINGS.add(new KeyBinding(ESCAPE, KEY_PRESSED, "Close"));
		MATERIALSTATE_SELECTOR_BINDINGS.add(new KeyBinding(SPACE, KEY_PRESSED, "Open"));
		MATERIALSTATE_SELECTOR_BINDINGS.add(new KeyBinding(ENTER, KEY_PRESSED, "Open"));
	}

	@Override
	protected void callAction(String name) {
		if(name.equals("Open")) {
			show();
		}
		else if(name.equals("Close")) {
			hide();
		}
		else
			super.callAction(name);
	}

	@Override
	public void onAutoHide() {
		MaterialStateSelector colorPicker = (MaterialStateSelector) getControl();
		MaterialStateSelectorSkin cpSkin = (MaterialStateSelectorSkin) colorPicker.getSkin();
		cpSkin.syncWithAutoUpdate();
		if(!colorPicker.isShowing())
			super.onAutoHide();
	}

}
