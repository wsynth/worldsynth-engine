/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.customobject;

import net.worldsynth.material.MaterialState;

public class Block {
	public int x;
	public int y;
	public int z;
	public MaterialState<?, ?> material;
	
	public Block(int x, int y, int z, MaterialState<?, ?> material) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.material = material;
	}
}
