/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.customobject;

public class LocatedCustomObject {
	private double x, y, z;
	private long seed;
	private CustomObject object;
	
	public LocatedCustomObject(double x, double y, double z, long seed) {
		this(x, y, z, seed, null);
	}
	
	public LocatedCustomObject(double x, double y, double z, long seed, CustomObject object) {
		setX(x);
		setY(y);
		setZ(z);
		setSeed(seed);
		setObject(object);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public long getSeed() {
		return seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}

	public CustomObject getObject() {
		return object;
	}

	public void setObject(CustomObject object) {
		this.object = object;
	}
	
	@Override
	public LocatedCustomObject clone() {
		if(object != null) {
			return new LocatedCustomObject(x, y, z, seed, object.clone());
		}
		return new LocatedCustomObject(x, y, z, seed);
	}
}
