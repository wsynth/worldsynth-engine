/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.customobject;

import java.io.File;
import java.io.IOException;

public abstract class CustomObjectFormat {
	
	/**
	 * Read object from file
	 * 
	 * @param file The object file to read
	 * @return The object read from the file
	 * @throws IOException
	 */
	public abstract CustomObject readObjectFromFile(File file) throws IOException;
	
	/**
	 * Write object to file
	 * 
	 * @param file The object file to write to
	 * @param object The object to write to file
	 * @return True if object was successfully written to file
	 * @throws IOException
	 */
	public abstract boolean writeObjectToFile(File file, CustomObject object) throws IOException;
	
	/**
	 * Getter for the object format name
	 * 
	 * @return The name of the object format
	 */
	public abstract String formatName();
	
	/**
	 * Getter for the file suffix
	 * 
	 * @return The file suffix used for the object format
	 */
	public abstract String formatSuffix();
}
