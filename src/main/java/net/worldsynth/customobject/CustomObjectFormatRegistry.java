/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.customobject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.worldsynth.addon.AddonLoader;

public class CustomObjectFormatRegistry {
	private static final Logger logger = LogManager.getLogger(CustomObjectFormatRegistry.class);
	
	private static final HashMap<String, CustomObjectFormat> formats = new HashMap<String, CustomObjectFormat>(); // Map<suffix, format>
	
	public CustomObjectFormatRegistry(AddonLoader addonLoader) {
		for(CustomObjectFormat format: addonLoader.getAddonCustomObjectFormats()) {
			registerFormat(format);
		}
	}
	
	private static void registerFormat(CustomObjectFormat format) {
		logger.info("Registering custom object format for \"" + format.formatName() + "\" and file suffix \"." + format.formatSuffix() + "\"");
		formats.put(format.formatSuffix().toLowerCase(), format);
	}
	
	public static List<CustomObjectFormat> getFormats() {
		return new ArrayList<CustomObjectFormat>(formats.values());
	}
	
	public static CustomObjectFormat getFormatForSuffix(String suffix) {
		return formats.get(suffix.toLowerCase());
	}
	
	public static CustomObjectFormat getFormatForFile(File file) {
		String fileName = file.getName();
		String suffix = fileName.substring(fileName.lastIndexOf(".")+1);
		return getFormatForSuffix(suffix);
	}
}
