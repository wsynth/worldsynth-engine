/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.common;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorldSynthDirectoryConfig {
	private static final Logger logger = LogManager.getLogger(WorldSynthDirectoryConfig.class);
	
	private File materialsDirectory;
	private File biomesDirectory;
	private File addonsDirectory;
	
	public WorldSynthDirectoryConfig(File worldSynthDirectory) {
		this(worldSynthDirectory, null, null, null);
	}
	
	public WorldSynthDirectoryConfig(File worldSynthDirectory, File addonDirectory, File materialsDirectory, File biomesDirectory) {
		this.addonsDirectory = initDirectory("addons", worldSynthDirectory, addonDirectory);
		this.materialsDirectory = initDirectory("materials", worldSynthDirectory, materialsDirectory);
		this.biomesDirectory = initDirectory("biomes", worldSynthDirectory, biomesDirectory);
	}
	
	private File initDirectory(String directoryName, File rootDirectory, File directory) {
		logger.info("Initializing " + directoryName + " directory");
		if(directory == null) {
			logger.info("No argument was provided for " + directoryName);
			directory = new File(rootDirectory, directoryName);
		}
		else {
			logger.info("Argument povided for " + directoryName);
		}
		logger.info(directoryName + " directory at " + directory.getAbsolutePath());
		if(!directory.exists()) {
			logger.warn("Directory at " + directory + " did not exist");
			directory.mkdir();
			logger.warn("Directory created directory at " + directory);
		}
		return directory;
	}
	
	public File getAddonDirectory() {
		return addonsDirectory;
	}
	
	public File getMaterialsDirectory() {
		return materialsDirectory;
	}
	
	public File getBiomesDirectory() {
		return biomesDirectory;
	}
}
