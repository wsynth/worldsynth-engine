/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.common.math;

public class Vector2d {
	double x, y;
	
	public Vector2d(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public double getX() {
		return x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getY() {
		return y;
	}
	
	public static Vector2d multiply(Vector2d v, double s) {
		return new Vector2d(v.x * s, v.y * s);
	}
	
	public static Vector2d normalize(Vector2d v) {
		double abs = abs(v);
		if(abs == 0.0) return multiply(v, 0.0);
		return multiply(v, 1/abs);
	}
	
	public static Vector2d normal(Vector2d v) {
		return new Vector2d(-v.y, v.x);
	}
	
	public static double abs(Vector2d v) {
		return Math.sqrt(v.x * v.x + v.y * v.y);
	}
	
	public static double dotProduct(Vector2d a, Vector2d b) {
		return a.x * b.x + a.y * b.y;
	}
	
	public static double angle(Vector2d a, Vector2d b) {
		return Math.acos(dotProduct(a, b) / (abs(a) * abs(b)));
	}
	
	public static Vector2d rotateDeg(Vector2d v, double angle) {
		return rotateRad(v, Math.toRadians(angle));
	}
	
	public static Vector2d rotateRad(Vector2d v, double angle) {
		return new Vector2d(v.x*Math.cos(angle) - v.y*Math.sin(angle), v.x*Math.sin(angle) + v.y*Math.cos(angle));
	}
}
