/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.common;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.worldsynth.addon.AddonLoader;
import net.worldsynth.addon.IAddonLoader;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.customobject.CustomObjectFormatRegistry;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.AbstractModuleRegister.ModuleEntry;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleMacro;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.module.NativeModuleRegister;
import net.worldsynth.modulewrapper.ModuleConnector;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.modulewrapper.ModuleWrapperIO;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.build.BuildStatus.BuildState;
import net.worldsynth.util.event.build.BuildStatusEvent;
import net.worldsynth.util.event.build.BuildStatusListener;

public class WorldSynthCore {
	
	public static final Logger LOGGER = LogManager.getLogger("worldsynth-engine");
	
	public static NativeModuleRegister moduleRegister;
	
	public WorldSynthCore(File worldSynthDirectory) {
		this(worldSynthDirectory, null);
	}
	
	public WorldSynthCore(WorldSynthDirectoryConfig worldSynthDirectoryConfig) {
		this(worldSynthDirectoryConfig, null);
	}
	
	public WorldSynthCore(File worldSynthDirectory, IAddonLoader[] injectedAddonLoaders) {
		this(new WorldSynthDirectoryConfig(worldSynthDirectory), injectedAddonLoaders);
	}
	
	public WorldSynthCore(WorldSynthDirectoryConfig worldSynthDirectoryConfig, IAddonLoader[] injectedAddonLoaders) {
		
		AddonLoader addonLoader = new AddonLoader(worldSynthDirectoryConfig.getAddonDirectory(), injectedAddonLoaders);
		
		//Initialize the biome registry
		LOGGER.info("Initializing biome registry");
		new BiomeRegistry(worldSynthDirectoryConfig.getBiomesDirectory(), addonLoader);
		//Initialize  the material registry
		LOGGER.info("Initializing material registry");
		new MaterialRegistry(worldSynthDirectoryConfig.getMaterialsDirectory(), addonLoader);
		
		//Initialize the module registry
		LOGGER.info("Initializing module registry");
		moduleRegister = new NativeModuleRegister(addonLoader);
		
		//Initialize the custom object format registry
		LOGGER.info("Initializing custom object format registry");
		new CustomObjectFormatRegistry(addonLoader);
	}
	
	public static AbstractDatatype getModuleOutput(Synth synth, ModuleWrapper wrapper, ModuleOutputRequest request, BuildStatusListener buildListener) {
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.REGISTERED, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.PREPARING_INPUTREQUESTS, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Get the input requests the module wants for building the output requested
		//and make sure all requests are unique instances.
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		for(Entry<String, ModuleInputRequest> entry: wrapper.module.getInputRequestsSuper(request).entrySet()) {
			if(entry == null) {
				continue;
			}
			inputRequests.put(entry.getKey(), new ModuleInputRequest(entry.getValue().getInput(), entry.getValue().getData().clone()));
		}
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.TRANSLATING_INPUTREQUESTS_TO_OUTPUTREQUESTS, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Translate the input requests to output requests
		HashMap<String, ModuleOutputRequest> outputRequests = new HashMap<String, ModuleOutputRequest>();
		HashMap<String, ModuleWrapper> outputRequestsModuleWrapper = new HashMap<String, ModuleWrapper>();
		for(Entry<String, ModuleInputRequest> entry: inputRequests.entrySet()) {
			//Transform inputrequests into output requests if possible
			ModuleWrapperIO input = wrapper.getWrapperIoByModuleIo(entry.getValue().getInput());
			ModuleConnector[] connectors = synth.getConnectorsByWrapperIo(input);
			if(connectors.length == 0) {
				continue;
			}
			ModuleConnector connector = connectors[0];
			ModuleWrapper requestedWrapper = connector.outputWrapper;
			ModuleOutput requestedModuleOutput = (ModuleOutput) connector.outputWrapperIo.getIO();
			
			ModuleOutputRequest newOutputRequest = new ModuleOutputRequest(requestedModuleOutput, entry.getValue().getData());
			if(entry.getValue().getData() instanceof DatatypeMultitype) {
				for(AbstractDatatype datatype: ((DatatypeMultitype) entry.getValue().getData()).getDatatypes()) {
					if(requestedModuleOutput.getData().getClass().equals(datatype.getClass())) {
						newOutputRequest = new ModuleOutputRequest(requestedModuleOutput, datatype);
						break;
					}
				}
			}
			outputRequests.put(entry.getKey(), newOutputRequest);
			outputRequestsModuleWrapper.put(entry.getKey(), requestedWrapper);
		}
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.PREPARING_INPUT_DATA, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Invoke the outputrequests
		HashMap<String, AbstractDatatype> data = new HashMap<String, AbstractDatatype>();
		for(Entry<String, ModuleOutputRequest> entry: outputRequests.entrySet()) {
			AbstractDatatype newData = getModuleOutput(synth, outputRequestsModuleWrapper.get(entry.getKey()), entry.getValue(), buildListener);
			if(newData != null) {
				data.put(entry.getKey(), newData);
			}
		}
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.BUILDING, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Build the module wrapped
		AbstractDatatype output = wrapper.module.buildModuleSuper(data, request);
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.DONE_BUILDING, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Return the data from the build
		return output;
	}
	
	public static AbstractModule constructModule(Class<? extends AbstractModule> moduleClass, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		if(moduleClass == ModuleMacro.class && wrapper != null) {
			return new ModuleMacro(wrapper);
		}
		Constructor<? extends AbstractModule> ct = moduleClass.getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		return moduleInstance;
	}
	
	public static AbstractModule constructModule(Class<? extends AbstractModule> moduleClass, Element moduleElement, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		if(moduleClass == ModuleMacro.class && wrapper != null) {
			return new ModuleMacro(wrapper);
		}
		Constructor<? extends AbstractModule> ct = moduleClass.getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		moduleInstance.fromElement(moduleElement);
		return moduleInstance;
	}
	
	public static AbstractModule constructModule(ModuleEntry moduleEntry, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		if(moduleEntry.getModuleClass() == ModuleMacro.class && wrapper != null) {
			return new ModuleMacro(wrapper);
		}
		Constructor<? extends AbstractModule> ct = moduleEntry.getModuleClass().getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		return moduleInstance;
	}
	
	public static AbstractModule constructModule(ModuleEntry moduleEntry, Element moduleElement, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		if(moduleEntry.getModuleClass() == ModuleMacro.class && wrapper != null) {
			return new ModuleMacro(wrapper);
		}
		Constructor<? extends AbstractModule> ct = moduleEntry.getModuleClass().getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		moduleInstance.fromElement(moduleElement);
		return moduleInstance;
	}
}
