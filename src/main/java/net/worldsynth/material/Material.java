/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import net.worldsynth.common.color.WsColor;

@JsonDeserialize(using = MaterialJsonDeserializer.class)
public class Material<M extends Material<M, S>, S extends MaterialState<M, S>> implements Comparable<Material<?, ?>> {
	
	@SuppressWarnings("rawtypes")
	public static final Material NULL = new MaterialBuilder("", "").isAir(true).createMaterial();
	
	protected String idName;
	protected String displayName;
	protected WsColor color;
	
	protected Texture texture; //Optional texture
	
	protected Set<String> tags; //Optional tag list for extra filtering
	
	protected boolean isAir;
	
	protected Map<String, List<String>> properties;
	protected Map<StateProperties, S> states;
	protected S defaultState;
	
	protected Material(
			String idName,
			String displayName,
			WsColor color,
			Texture texture,
			Set<String> tags,
			boolean isAir,
			Map<String, List<String>> properties,
			Set<S> states)
	{
		this.idName = idName;
		this.displayName = displayName;
		this.color = color;
		this.texture = texture;
		this.tags = tags;
		this.isAir = isAir;
		this.properties = properties;
		
		if(states == null || states.isEmpty()) {
			throw new IllegalArgumentException("States cannot be null or empty");
		}
		if((properties == null || properties.isEmpty()) && states.size() != 1) {
			throw new IllegalArgumentException("Material must have exactly one state when it has no properties");
		}
		
		this.states = new LinkedHashMap<StateProperties, S>();
		for(S state: states) {
			if(properties == null ? !state.getProperties().isEmpty() : !state.getProperties().isPossibilityIn(properties)) {
				throw new IllegalArgumentException("State properties must be made up of a complete set of possible material properties");
			}
			if(this.states.containsKey(state.properties)) {
				throw new IllegalArgumentException("All states of a material must have unique state properties");
			}
			
			state.setMaterial((M) this);
			this.states.put(state.getProperties(), state);
			if(state.isDefault()) {
				defaultState = state;
			}
		}
		if(defaultState == null) {
			throw new IllegalArgumentException("Material must have a default state");
		}
	}
	
	public String getIdName() {
		return idName;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public WsColor getWsColor() {
		return color;
	}
	
	public Color getFxColor() {
		return getWsColor().getFxColor();
	}
	
	public Texture getTexture() {
		return texture;
	}
	
	public Image getTextureImage() {
		if(texture != null) {
			return texture.getTextureImage();
		}
		return null;
	}
	
	public Set<String> getMaterialTags() {
		return tags;
	}
	
	public boolean isAir() {
		return isAir;
	}
	
	public Map<String, List<String>> getProperties() {
		return properties;
	}
	
	public ArrayList<S> getStates() {
		return new ArrayList<S>(states.values());
	}
	
	public S getState(StateProperties propertiesKey) {
		return states.get(propertiesKey);
	}
	
	public S getDefaultState() {
		return defaultState;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		else if(obj instanceof Material) {
			Material<?, ?> anotherMaterial = (Material<?, ?>) obj;
			if(!(idName == null ? anotherMaterial.idName == null : idName.equals(anotherMaterial.idName))) return false;
			else if(!(displayName == null ? anotherMaterial.displayName == null : displayName.equals(anotherMaterial.displayName))) return false;
			else if(!(color == null ? anotherMaterial.color == null : color.equals(anotherMaterial.color))) return false;
			else if(!(texture == null ? anotherMaterial.texture == null : texture.equals(anotherMaterial.texture))) return false;
			else if(!(tags == null ? anotherMaterial.tags == null : tags.equals(anotherMaterial.tags))) return false;
			else if(isAir != anotherMaterial.isAir) return false;
			else if(!(properties == null ? anotherMaterial.properties == null : properties.equals(anotherMaterial.properties))) return false;
			else if(!(states == null ? anotherMaterial.states == null : states.equals(anotherMaterial.states))) return false;
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getDisplayName();
	}

	@Override
	public int compareTo(Material<?, ?> comp) {
		return getDisplayName().compareTo(comp.getDisplayName());
	}
}
