/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import net.worldsynth.common.color.WsColor;

public class MaterialState<M extends Material<M, S>, S extends MaterialState<M, S>> {
	/**
	 * Parent material that this is a state of
	 */
	private M material;
	protected StateProperties properties;
	protected String idName;
	protected boolean isDefault;
	
	//Any of the following that are non-null overrides the corresponding value in the parent material.
	//Otherwise the value from the parent material is returned by the getter function.
	protected String displayName;
	protected WsColor color;
	protected Texture texture;
	
	protected MaterialState(
			StateProperties properties,
			boolean isDefault,
			String displayName,
			WsColor color,
			Texture texture) {
		
		if(properties == null) throw new NullPointerException("State properties cannot be null");
		this.properties = properties;
		
		this.isDefault = isDefault;
		
		this.displayName = displayName;
		this.color = color;
		this.texture = texture;
	}
	
	void setMaterial(M material) {
		if(this.material != null) {
			throw new IllegalStateException("Material can only be set once");
		}
		if(material == null) {
			throw new IllegalArgumentException("Material cannot be null");
		}
		
		this.material = material;
		idName = material.getIdName();
		if(properties != null && !properties.isEmpty()) {
			idName += ":";
			idName += properties;
		}
	}
	
	public M getMaterial() {
		return material;
	}
	
	public String getIdName() {
		return idName;
	}
	
	public StateProperties getProperties() {
		return properties;
	}
	
	public boolean isDefault() {
		return isDefault;
	}
	
	public String getDisplayName() {
		if(displayName != null) return displayName;
		return material.getDisplayName() + ((properties != null && !properties.isEmpty()) ? ("[" + properties.toString() + "]") : "");
	}
	
	public WsColor getWsColor() {
		if(color != null) return color;
		return material.getWsColor();
	}
	
	public Color getFxColor() {
		return getWsColor().getFxColor();
	}
	
	public Texture getTexture() {
		if(texture != null) return texture;
		return material.getTexture();
	}
	
	public Image getTextureImage() {
		if(texture != null) {
			return texture.getTextureImage();
		}
		return material.getTextureImage();
	}
	
	public boolean isAir() {
		return material.isAir();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		else if(obj instanceof MaterialState) {
			MaterialState<?, ?> anotherState = (MaterialState<?, ?>) obj;
			if(!(material == null ? anotherState.material == null : material.equals(anotherState.material))) return false;
			else if(!(idName == null ? anotherState.idName == null : idName.equals(anotherState.idName))) return false;
			else if(isDefault != anotherState.isDefault) return false;
			else if(!(displayName == null ? anotherState.displayName == null : displayName.equals(anotherState.displayName))) return false;
			else if(!(color == null ? anotherState.color == null : color.equals(anotherState.color))) return false;
			else if(!(texture == null ? anotherState.texture == null : texture.equals(anotherState.texture))) return false;
			else if(!(properties == null ? anotherState.properties == null : properties.equals(anotherState.properties))) return false;
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getDisplayName();
	}
}
