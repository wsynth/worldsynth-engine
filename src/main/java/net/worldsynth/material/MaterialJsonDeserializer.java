/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class MaterialJsonDeserializer<M extends Material<M, S>, S extends MaterialState<M, S>> extends StdDeserializer<M> {
	private static final long serialVersionUID = -9132730928195487130L;

	protected MaterialJsonDeserializer() {
		this(null);
	}
	
	protected MaterialJsonDeserializer(Class<?> vc) {
		super(vc);
	}
	
	@Override
	public M deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		String idName = (String) ctxt.findInjectableValue("idName", null, null);
		File materialsFile = (File) ctxt.findInjectableValue("materialsFile", null, null);
		return new MaterialBuilder<M, S>(idName, node, materialsFile).createMaterial();
	}
}
