/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class StateProperties {
	private final HashMap<String, Property> properties = new HashMap<String, Property>();
	
	public StateProperties(Property... properties) {
		this(Arrays.asList(properties));
	}
	
	public StateProperties(List<Property> properties) {
		if(properties != null) {
			for(Property p: properties) {
				this.properties.put(p.getName(), p);
			}
		}
	}
	
	public StateProperties(Map<String, String> properties) {
		for(Entry<String, String> prop: properties.entrySet()) {
			this.properties.put(prop.getKey(), new Property(prop.getKey(), prop.getValue()));
		}
	}
	
	void addProperty(Property property) {
		properties.put(property.getName(), property);
	}
	
	public boolean isEmpty() {
		return properties.isEmpty();
	}
	
	public List<Property> asList() {
		return new ArrayList<Property>(properties.values());
	}
	
	public boolean isPossibilityIn(Map<String, List<String>> properties) {
		if(this.properties.size() != properties.size()) return false;
		for(String key: this.properties.keySet()) {
			if(!properties.containsKey(key)) return false;
			if(!properties.get(key).contains(this.properties.get(key).getValue())) return false;
		}
		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		else if(obj instanceof StateProperties) {
			StateProperties anotherPropertiesKey = (StateProperties) obj;
			return anotherPropertiesKey.properties.equals(properties);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		int h = 0;
		for(Entry<String, Property> e: properties.entrySet()) {
			h += e.hashCode();
		}
		return h;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		boolean firstProp = true;
		for(String propKey: properties.keySet()) {
			if(!firstProp) stringBuilder.append(",");
			stringBuilder.append(properties.get(propKey));
			firstProp = false;
		}
		return stringBuilder.toString();
	}
}
