/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;

public class DatatypeMaterialmap extends AbstractDatatype {
	
	private MaterialState<?, ?>[][] materialmap;
	
	/**
	 * Corner coordinate
	 */
	public double x, z;
	
	/**
	 * Unit size of the materialmap
	 */
	public double width, length;
	
	/**
	 * The resolutions of units per materialmap point
	 */
	public double resolution;
	
	/**
	 * The number of points in the materialmap
	 */
	public int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeMaterialmap() {
	}
	
	/**
	 * @param x
	 * @param z
	 * @param width 
	 * @param length
	 * @param resolution The unit-distance between the points in the materialmap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeMaterialmap(double x, double z, double width, double length, double resolution) {
		this.x = x;
		this.z = z;
		this.width = width;
		this.length = length;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(width / resolution);
		mapPointsLength = (int) Math.ceil(length / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param z
	 * @param width
	 * @param length
	 */
	public DatatypeMaterialmap(double x, double z, double width, double length) {
		this(x, z, width, length, 1);
	}
	
	public MaterialState<?, ?>[][] getMaterialmap() {
		return materialmap;
	}
	
	public void setMaterialmap(MaterialState<?, ?>[][] materialmap) {
		int paramWidth = materialmap.length;
		int paramLength = materialmap[0].length;
		if(paramWidth != mapPointsWidth || paramLength != mapPointsLength) {
			throw new IllegalArgumentException("Heightmap data has wrong size. Expected a " + mapPointsWidth + "x" + mapPointsLength + " map, got a " + paramWidth + "x" + paramLength  + " map.");
		}
		
		//Replace all null entries with default air
		MaterialState<?, ?> air = MaterialRegistry.getDefaultAir().getDefaultState();
		for(int u = 0; u < mapPointsWidth; u++) {
			for(int v = 0; v < mapPointsLength; v++) {
				if(materialmap[u][v] == null) materialmap[u][v] = air;
			}
		}
		
		this.materialmap = materialmap;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(255, 170, 0);
	}

	@Override
	public String getDatatypeName() {
		return "Materialmap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeMaterialmap dmm = new DatatypeMaterialmap(x, z, width, length, resolution);
		if(materialmap != null) {
			dmm.materialmap = materialmap.clone();
		}
		return dmm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeMaterialmap(x, z, width, length, resolution);
	}
}
