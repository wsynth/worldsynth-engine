/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeValuespace extends AbstractDatatype {
	
	public float[][][] valuespace;
	
	/**
	 * Corner coordinate
	 */
	public double x, y, z;
	
	/**
	 * Unit size of the valuespace
	 */
	public double width, height, length;
	
	/**
	 * The resolutions of units per valuespace point
	 */
	public double resolution;
	
	/**
	 * The number of points in the valuespace
	 */
	public int spacePointsWidth, spacePointsHeight, spacePointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeValuespace() {
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param length
	 * @param resolution The unit-distance between the points in the valuespace. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeValuespace(double x, double y, double z, double width, double height, double length, double resolution) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.length = length;
		this.resolution = resolution;
		
		spacePointsWidth = (int) Math.ceil(width / resolution);
		spacePointsHeight = (int) Math.ceil(height / resolution);
		spacePointsLength = (int) Math.ceil(length / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param lenght
	 */
	public DatatypeValuespace(double x, double y, double z, double width, double height, double lenght) {
		this(x, y, z, width, height, lenght, 1);
	}
	
	public float[][][] getValuespace() {
		return valuespace;
	}
	
	public void setValuespace(float[][][] valuespace) {
		int paramWidth = valuespace.length;
		int paramHeight = valuespace[0].length;
		int paramLength = valuespace[0][0].length;
		if(paramWidth != spacePointsWidth || paramHeight != spacePointsHeight || paramLength != spacePointsLength) {
			throw new IllegalArgumentException("Valuespace data has wrong size. Expected a " + spacePointsWidth + "x" + spacePointsHeight + "x" + spacePointsLength+ " map, got a " + paramWidth + "x" + paramHeight + "x" + paramLength  + " space.");
		}
		this.valuespace = valuespace;
	}
	
	public float getLocalValue(int x, int y, int z) {
		return valuespace[x][y][z];
	}
	
	public float getLocalLerpValue(double x, double y, double z) {
		int x1 = (int) x;
		int x2 = x1+1;
		x2 = Math.min(x2, spacePointsWidth-1);
		int y1 = (int) y;
		int y2 = y1+1;
		y2 = Math.min(y2, spacePointsHeight-1);
		int z1 = (int) z;
		int z2 = z1+1;
		z2 = Math.min(z2, spacePointsLength-1);
		
		double u = x - Math.floor(x);
		double v = y - Math.floor(y);
		double w = z - Math.floor(z);
		
		double valy1 = (1.0-w)*(1.0-u)*valuespace[x1][y1][z1] + (1.0-w)*u*valuespace[x2][y1][z1] + w*(1.0-u)*valuespace[x1][y1][z2] + w*u*valuespace[x2][y1][z2];
		double valy2 = (1.0-w)*(1.0-u)*valuespace[x1][y2][z1] + (1.0-w)*u*valuespace[x2][y2][z1] + w*(1.0-u)*valuespace[x1][y2][z2] + w*u*valuespace[x2][y2][z2];
		double val = (1.0-v)*valy1 + v*valy2;
		
		return (float) val;
	}
	
	public float getGlobalValue(double x, double z) {
		x = (x - this.x) / resolution;
		y = (y - this.y) / resolution;
		z = (z - this.z) / resolution;
		
		return valuespace[(int) x][(int) y][(int) z];
	}
	
	public float getGlobalLerpValue(double x, double y, double z) {
		double localX = (x - this.x) / resolution;
		double localY = (y - this.y) / resolution;
		double localZ = (z - this.z) / resolution;
		
		return getLocalLerpValue(localX, localY, localZ);
	}
	
	public boolean isLocalContained(int x, int y, int z) {
		if(x < 0 || x >= spacePointsWidth || y < 0 || y >= spacePointsHeight || z < 0 || z >= spacePointsLength) {
			return false;
		}
		return true;
	}
	
	public boolean isGlobalContained(double x, double y, double z) {
		if(x < this.x || x >= this.x + width || y < this.y || y >= this.y + height || z < this.z || z >= this.z + length) {
			return false;
		}
		return true;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 255, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Valuespace";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeValuespace dvs = new DatatypeValuespace(x, y, z, width, height, length, resolution);
		if(valuespace != null) {
			float[][][] vs = valuespace.clone();
			dvs.valuespace = vs;
		}
		return dvs;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeValuespace(x, y, z, width, height, length, resolution);
	}
}
