/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeVectormap extends AbstractDatatype {
	
	public float[][][] vectorField;
	
	/**
	 * Corner coordinate
	 */
	public double x, z;
	
	/**
	 * Unit size of the vectorfield
	 */
	public double width, length;
	
	/**
	 * The resolutions of units per vectorfield point
	 */
	public double resolution;
	
	/**
	 * The number of points in the vectorfield
	 */
	public int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeVectormap() {
	}
	
	/**
	 * @param x
	 * @param z
	 * @param width 
	 * @param length
	 * @param resolution The unit-distance between the points in the vectorfield. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeVectormap(double x, double z, double width, double length, double resolution) {
		this.x = x;
		this.z = z;
		this.width = width;
		this.length = length;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(width / resolution);
		mapPointsLength = (int) Math.ceil(length / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param z
	 * @param width
	 * @param length
	 */
	public DatatypeVectormap(double x, double z, double width, double length) {
		this(x, z, width, length, 1);
	}
	
	public float[] getLocalVector(int x, int z) {
		return vectorField[x][z];
	}
	
	public float[] getLocalLerpVector(double x, double z) {
		int x1 = (int) x;
		int x2 = x1+1;
		x2 = Math.min(x2, mapPointsWidth-1);
		int z1 = (int) z;
		int z2 = z1+1;
		z2 = Math.min(z2, mapPointsLength-1);
		
		double u = x - Math.floor(x);
		double v = z - Math.floor(z);
		
		float xVal = (float) ((1.0-v)*(1.0-u)*vectorField[x1][z1][0] + (1.0-v)*u*vectorField[x2][z1][0] + v*(1.0-u)*vectorField[x1][z2][0] + v*u*vectorField[x2][z2][0]);
		float zVal = (float) ((1.0-v)*(1.0-u)*vectorField[x1][z1][1] + (1.0-v)*u*vectorField[x2][z1][1] + v*(1.0-u)*vectorField[x1][z2][1] + v*u*vectorField[x2][z2][1]);
		
		return new float[]{xVal, zVal};
	}
	
	public float[] getGlobalVector(double x, double z) {
		x = (x - this.x) / resolution;
		z = (z - this.z) / resolution;
		
		return vectorField[(int) x][(int) z];
	}
	
	public float[] getGlobalLerpVector(double x, double z) {
		double localX = (x - this.x) / resolution;
		double localZ = (z - this.z) / resolution;
		
		return getLocalLerpVector(localX, localZ);
	}
	
	public boolean isLocalContained(int x, int z) {
		if(x < 0 || x >= mapPointsWidth || z < 0 || z >= mapPointsLength) {
			return false;
		}
		return true;
	}
	
	public boolean isGlobalContained(double x, double z) {
		if(x < this.x || x >= this.x + width || z < this.z || z >= this.z + length) {
			return false;
		}
		return true;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(146, 0, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Vectormap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeVectormap dhm = new DatatypeVectormap(x, z, width, length, resolution);
		if(vectorField != null) {
			float[][][] hm = vectorField.clone();
			dhm.vectorField = hm;
		}
		return dhm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeVectormap(x, z, width, length, resolution);
	}
}
