/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import java.util.HashSet;

import javafx.scene.paint.Color;
import net.worldsynth.biome.Biome;

public class DatatypeBiomemap extends AbstractDatatype {
	
	private Biome[][] biomemap;
	
	/**
	 * Corner coordinate
	 */
	public double x, z;
	
	/**
	 * Unit size of the biomemap
	 */
	public double width, length;
	
	/**
	 * The resolutions of units per biomemap point
	 */
	public double resolution;
	
	/**
	 * The number of points in the biomemap
	 */
	public int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeBiomemap() {
	}
	
	/**
	 * @param x
	 * @param z
	 * @param width 
	 * @param length
	 * @param resolution The unit-distance between the points in the biomemap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeBiomemap(double x, double z, double width, double length, double resolution) {
		this.x = x;
		this.z = z;
		this.width = width;
		this.length = length;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(width / resolution);
		mapPointsLength = (int) Math.ceil(length / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param z
	 * @param width
	 * @param length
	 */
	public DatatypeBiomemap(double x, double z, double width, double length) {
		this(x, z, width, length, 1);
	}
	
	public Biome[][] getBiomemap() {
		return biomemap;
	}
	
	public void setBiomemap(Biome[][] biomemap) {
		int paramWidth = biomemap.length;
		int paramLength = biomemap[0].length;
		if(paramWidth != mapPointsWidth || paramLength != mapPointsLength) {
			throw new IllegalArgumentException("Heightmap data has wrong size. Expected a " + mapPointsWidth + "x" + mapPointsLength + " map, got a " + paramWidth + "x" + paramLength  + " map.");
		}
		
		//Check all entries for null value
		for(int u = 0; u < mapPointsWidth; u++) {
			for(int v = 0; v < mapPointsLength; v++) {
				if(biomemap[u][v] == null) {
					throw new IllegalArgumentException("Biomemap cannot contain null as a value");
				};
			}
		}
		
		this.biomemap = biomemap;
	}
	
	public Biome[] getBiomelist() {
		HashSet<Biome> biomeset = new HashSet<Biome>();

		for(int u = 0; u < biomemap.length; u++) {
			for(int v = 0; v < biomemap[0].length; v++) {
				biomeset.add(biomemap[u][v]);
			}
		}
		
		return biomeset.toArray(new Biome[biomeset.size()]);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 85, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Biomemap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeBiomemap dbm = new DatatypeBiomemap(x, z, width, length, resolution);
		if(biomemap != null) {
			dbm.biomemap = biomemap.clone();
		}
		return dbm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeBiomemap(x, z, width, length, resolution);
	}
}
