/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.synth;

import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.synth.io.Element;

public class SubSynth extends Synth {
	
	private Synth parrentSynth;
	
	public SubSynth(String name, Synth parrentSynth) {
		super(name);
		this.parrentSynth = parrentSynth;
	}
	
	public SubSynth(Element element, Synth parrentSynth) {
		super("");
		this.parrentSynth = parrentSynth;
		fromElement(element, parrentSynth);
	}
	
	public Synth getParrentSynth() {
		return parrentSynth;
	}
	
	public WorldExtentManager getExtentManager() {
		return parrentSynth.getExtentManager();
	}
}
