/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.synth.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import net.worldsynth.synth.SubSynth;
import net.worldsynth.synth.Synth;

public class ProjectReader {
	
	public static Synth readSynthFromFile(File file) {
		//TODO Errorhandeling and error feedback
		String documentformat = "";
		
		try {
			FileInputStream fis = new FileInputStream(file);
			byte[] bytebuffer = new byte[(int) file.length()];
			fis.read(bytebuffer);
			fis.close();
			documentformat = new String(bytebuffer);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	
		Element rootElement = new Element(documentformat);
		
		return new Synth(rootElement);
	}
	
	public static SubSynth readSubSynthFromFile(File file, Synth memberSynth) {
		//TODO Errorhandeling and error feedback
		String documentformat = "";
		
		try {
			FileInputStream fis = new FileInputStream(file);
			byte[] bytebuffer = new byte[(int) file.length()];
			fis.read(bytebuffer);
			fis.close();
			documentformat = new String(bytebuffer);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	
		Element rootElement = new Element(documentformat);
		
		return new SubSynth(rootElement, memberSynth);
	}
}
