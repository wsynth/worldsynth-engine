/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.fileio;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FileParameter;
import net.worldsynth.parameter.WorldExtentParameter;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleColormapExporter extends AbstractModule {

	private WorldExtentParameter exportExtent = new WorldExtentParameter("extent", "Export extent", null, this);
	private FileParameter exportFile = new FileParameter("file", "Save file", null, new File(System.getProperty("user.home") + "/colormap_export.png"), true, false, new ExtensionFilter("PNG", "*.png"));
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				exportExtent,
				exportFile
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("colormap");
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeColormap colormapRequestData = (DatatypeColormap) outputRequest.data;
		
		inputRequests.put("colormap", new ModuleInputRequest(getInput(0), colormapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Colormap exporter";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.EXPORTER_COLORMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeColormap(), "Colormap")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Troughput", false)
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ParameterUiElement<WorldExtent> parameterExtent = this.exportExtent.getUi();
		ParameterUiElement<File> parameterFile = this.exportFile.getUi();
		
		Button exportButton = new Button("Export colormap");
		exportButton.setOnAction(e -> {
			if(parameterExtent.getValue() == null) {
				return;
			}
			
			float[][][] colormap = getColormap(parameterExtent.getValue());
			writeImageToFile(colormap, parameterFile.getValue());
		});
		
		parameterExtent.addToGrid(pane, 0);
		parameterFile.addToGrid(pane, 1);
		pane.add(exportButton, 1, 2);
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			parameterExtent.applyUiValue();
			parameterFile.applyUiValue();
		};
		
		return applyHandler;
	}

	private float[][][] getColormap(WorldExtent extent) {
		ModuleInputRequest request = new ModuleInputRequest(getInput(0), new DatatypeColormap(extent.getX(), extent.getZ(), extent.getWidth(), extent.getLength()));
		DatatypeColormap cmd = (DatatypeColormap) buildInputData(request);
		return cmd.colorMap;
	}

	private void writeImageToFile(float[][][] colormap, File f) {
		try {
			BufferedImage image = new BufferedImage(colormap.length, colormap[0].length, BufferedImage.TYPE_INT_RGB);
			for(int x = 0; x < colormap.length; x++) {
				System.out.println("Converting to buffer: " + (int)(100.0f*(float)x/(float)colormap.length) + "%");
				for(int y = 0; y < colormap[0].length; y++) {
					Color c = new Color(colormap[x][y][0], colormap[x][y][1], colormap[x][y][2]);
					image.setRGB(x, y, c.getRGB());
				}
			}
			ImageIO.write(image, "png", f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
