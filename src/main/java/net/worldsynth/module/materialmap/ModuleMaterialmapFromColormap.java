/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.materialmap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.list.MaterialStateListParameter;

public class ModuleMaterialmapFromColormap extends AbstractModule {
	
	MaterialStateListParameter palette = new MaterialStateListParameter("palette", "Palette", null, Arrays.asList(MaterialRegistry.getDefaultMaterial().getDefaultState()));
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				palette
		};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeMaterialmap requestData = (DatatypeMaterialmap) request.data;
		
		//----------READ INPUTS----------//
		
		List<MaterialState<?, ?>> palette = this.palette.getValueList();
		if(palette.size() == 0) {
			return null;
		}
		
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][][] inputColorMap = ((DatatypeColormap) inputs.get("input")).colorMap;
		
		//----------BUILD----------//
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		MaterialState<?, ?>[][] materialmap = new MaterialState<?, ?>[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float r = inputColorMap[u][v][0];
				float g = inputColorMap[u][v][1];
				float b = inputColorMap[u][v][2];
				materialmap[u][v] = selectMaterialFromPalette(r, g, b, palette);
			}
		}
		
		requestData.setMaterialmap(materialmap);
		
		return requestData;
	}
	
	private MaterialState<?, ?> selectMaterialFromPalette(float r, float g, float b, List<MaterialState<?, ?>> palette) {
		double closestDist = -1;
		MaterialState<?, ?> closestMaterial = palette.get(0);
		
		for(int i = 0; i < palette.size(); i++) {
			MaterialState<?, ?> m = palette.get(i);
			float pr = (float) m.getWsColor().getRed();
			float pg = (float) m.getWsColor().getGreen();
			float pb = (float) m.getWsColor().getBlue();
			//Difference from color ideal
			double d = Math.sqrt(Math.pow(r - pr, 2) + Math.pow(g - pg, 2) + Math.pow(b - pb, 2));
			
			if(closestDist > d || closestDist < 0) {
				closestMaterial = palette.get(i);
				closestDist = d;
			}
		}
		
		return closestMaterial;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeMaterialmap materialmapRequestData = (DatatypeMaterialmap) outputRequest.data;
		
		DatatypeColormap colormapRequestData = new DatatypeColormap(materialmapRequestData.x, materialmapRequestData.z, materialmapRequestData.width, materialmapRequestData.length, materialmapRequestData.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), colormapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Materialmap from colormap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_MATERIALMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeColormap(), "Primary input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeMaterialmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
