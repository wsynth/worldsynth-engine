/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.biomemap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.biome.Biome;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;

public class ModuleBiomemapSelector extends AbstractModule {
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBiomemap requestData = (DatatypeBiomemap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		if(!inputs.containsKey("selector") || !inputs.containsKey("primary") || !inputs.containsKey("secondary")) {
			//If any of the inputs are not available, there is not enough input and then just return null
			return null;
		}
		
		float[][] inputMap0 = ((DatatypeHeightmap) inputs.get("selector")).getHeightmap();
		Biome[][] inputMap1 = ((DatatypeBiomemap) inputs.get("primary")).getBiomemap();
		Biome[][] inputMap2 = ((DatatypeBiomemap) inputs.get("secondary")).getBiomemap();
		
		Biome[][] map = new Biome[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				
				Biome bid = inputMap2[u][v];
				if(inputMap0[u][v] > 0) {
					bid = inputMap1[u][v];
				}
				map[u][v] = bid;
			}
		}
		
		requestData.setBiomemap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBiomemap biomemapRequestData = (DatatypeBiomemap) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(biomemapRequestData.x, biomemapRequestData.z, biomemapRequestData.width, biomemapRequestData.length, biomemapRequestData.resolution);
		
		inputRequests.put("selector", new ModuleInputRequest(getInput(0), heightmapRequestData));
		
		inputRequests.put("primary", new ModuleInputRequest(getInput(1), biomemapRequestData));
		inputRequests.put("secondary", new ModuleInputRequest(getInput(2), biomemapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Selector";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER_BIOMEMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Selector input"),
				new ModuleInput(new DatatypeBiomemap(), "Primary biomemap"),
				new ModuleInput(new DatatypeBiomemap(), "Secondary biomemap")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBiomemap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
