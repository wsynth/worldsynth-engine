/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import net.worldsynth.datatype.AbstractDatatype;

/**
 * Used in the process of building a node tree, to pass a request for data from a module.
 * This is the request received by a module for it to determine what data to build and what
 * inputs it needs to build, so it can construct the corresponding inputrequests to send out.
 */
public class ModuleOutputRequest {
	public ModuleOutput output;
	public AbstractDatatype data;
	
	public ModuleOutputRequest(ModuleOutput output, AbstractDatatype data) {
		this.output = output;
		this.data = data;
	}
}
