/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.IntegerParameter;

public class ModuleHeightmapTerrace extends AbstractModule {
	
	private EnumParameter<TerraceType> type = new EnumParameter<ModuleHeightmapTerrace.TerraceType>("type", "Terrace type", null, TerraceType.class, TerraceType.BASIC);
	private IntegerParameter levels = new IntegerParameter("levels", "Terrace levels", null, 10, 1, Integer.MAX_VALUE, 1, 100);
	private DoubleParameter shape = new DoubleParameter("shape", "Terrace shape", null, 0.0, 0.0, 1.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				type,
				levels,
				shape
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//

		TerraceType terraceType = this.type.getValue();
		int terraceLevels = this.levels.getValue();
		double shape = this.shape.getValue();
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in terrace modulation map
		float[][] terraceModulationMap = null;
		if(inputs.get("tmod") != null) {
			terraceModulationMap = ((DatatypeHeightmap) inputs.get("tmod")).getHeightmap();
		}
		
		//Read in offset modulation map
		float[][] offsetModulationMap = null;
		if(inputs.get("omod") != null) {
			offsetModulationMap = ((DatatypeHeightmap) inputs.get("omod")).getHeightmap();
		}
		
		//Read in mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] terracedMap = new float[mpw][mpl];
		
		//Has both modulation maps
		if(terraceModulationMap != null && offsetModulationMap != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = (float) terrace(inputMap[u][v], terraceModulationMap[u][v], offsetModulationMap[u][v], terraceLevels, terraceType, shape);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					terracedMap[u][v] = o;
				}
			}
		}
		//Has terrace modulation map
		else if(terraceModulationMap != null && offsetModulationMap == null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = (float) terrace(inputMap[u][v], terraceModulationMap[u][v], 0.0, terraceLevels, terraceType, shape);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					terracedMap[u][v] = o;
				}
			}
		}
		//Has terrace offset map
		else if(terraceModulationMap == null && offsetModulationMap != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = (float) terrace(inputMap[u][v], 1.0, offsetModulationMap[u][v], terraceLevels, terraceType, shape);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					terracedMap[u][v] = o;
				}
			}
		}
		//No modulation map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = (float) terrace(inputMap[u][v], 1.0, 0.0, terraceLevels, terraceType, shape);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					terracedMap[u][v] = o;
				}
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					terracedMap[u][v] = terracedMap[u][v] * mask[u][v] + inputMap[u][v] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.setHeightmap(terracedMap);
		
		return requestData;
	}
	
	private double terrace(double height, double terraceModulation, double offsetModulation, double levels, TerraceType type, double shape) {
		height -= offsetModulation;
		levels *= 0.3 + terraceModulation*0.7;
		double levelRes = 1.0 / levels;
		double level = Math.floor(height/levelRes);
		double levelDivision = (height*levels - level);
		shape *= 0.5;
		if(type == TerraceType.BASIC) {
			return level*levelRes + offsetModulation;
		}
		else if(type == TerraceType.SHARP) {
			if(levelDivision < shape) {
				return level * levelRes + levelDivision/(2.0*shape) * levelRes + offsetModulation;
			}
			else if(levelDivision < (1.0 - shape)) {
				return level * levelRes + 0.5 * levelRes + offsetModulation;
			}
			else {
				return level * levelRes + (levelDivision + 2.0*shape - 1)/(2.0*shape) * levelRes + offsetModulation;
			}
		}
		return 0.0f;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("tmod", new ModuleInputRequest(getInput(1), outputRequest.data));
		inputRequests.put("omod", new ModuleInputRequest(getInput(2), outputRequest.data));
		inputRequests.put("mask", new ModuleInputRequest(getInput(3), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Terrace";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeHeightmap(), "Terrace modulation"),
				new ModuleInput(new DatatypeHeightmap(), "Offset modulation"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum TerraceType {
		BASIC, SHARP;
	}
}
