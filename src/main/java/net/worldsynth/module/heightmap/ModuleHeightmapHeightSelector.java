/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;

public class ModuleHeightmapHeightSelector extends AbstractModule {
	
	private FloatParameter lowSelect = new FloatParameter("lowselect", "Low select", null, 0.0f, 0.0f, 1.0f, 256.0f);
	private FloatParameter highSelect = new FloatParameter("highselect", "High select", null, 1.0f, 0.0f, 1.0f, 256.0f);
	
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				highSelect,
				lowSelect
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float highSelect = this.highSelect.getValue();
		float lowSelect = this.lowSelect.getValue();
		
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();

		if(inputs.get("high") != null) {
			highSelect = (float) ((DatatypeScalar) inputs.get("high")).data;
		}
		
		if(inputs.get("low") != null) {
			lowSelect = (float) ((DatatypeScalar) inputs.get("low")).data;
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float i0 = inputMap[u][v];
				float o = select(i0, lowSelect, highSelect);
				o = Math.min(o, 1);
				map[u][v] = o;
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	private float select(float height, float lowSelect , float highSelect) {
		if(height <= highSelect && height >= lowSelect) height = 1.0f;
		else {
			height = 0.0f;
		}
		return height;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("high", new ModuleInputRequest(getInput(1), new DatatypeScalar()));
		inputRequests.put("low", new ModuleInputRequest(getInput(2), new DatatypeScalar()));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Height select";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.SELECTOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "High select"),
				new ModuleInput(new DatatypeScalar(), "Low select")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
