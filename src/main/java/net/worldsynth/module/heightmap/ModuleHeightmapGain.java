/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.DoubleParameter;

public class ModuleHeightmapGain extends AbstractModule {

	private DoubleParameter gain = new DoubleParameter("gain", "Gain", null, 1.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 10.0);
	private DoubleParameter offset = new DoubleParameter("offset", "Offset", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1.0, 1.0, 256.0);
	private BooleanParameter gainCentered = new BooleanParameter("gaincentered", "Gain centered", null, false);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				gain,
				offset,
				gainCentered
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in gain
		double gain = this.gain.getValue();
		float[][] gainMap = null;
		if(inputs.get("gain") != null) {
			if(inputs.get("gain") instanceof DatatypeScalar) {
				gain = ((DatatypeScalar) inputs.get("gain")).data;
			}
			else {
				gainMap = ((DatatypeHeightmap) inputs.get("gain")).getHeightmap();
			}
		}
		
		//Read in offset
		double offset = this.offset.getValue();
		float[][] offsetMap = null;
		if(inputs.get("offset") != null) {
			if(inputs.get("offset") instanceof DatatypeScalar) {
				offset = ((DatatypeScalar) inputs.get("offset")).data / 256.0;
			}
			else {
				offsetMap = ((DatatypeHeightmap) inputs.get("offset")).getHeightmap();
			}
		}
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] gainedMap = new float[mpw][mpl];
		
		float preOffset = 0.0f;
		if(this.gainCentered.getValue()) {
			preOffset = -0.5f;
		}
		
		//Has both gain and offset map
		if(gainMap != null && offsetMap != null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					gainedMap[u][v] = gain(inputMap[u][v], preOffset, gainMap[u][v]*gain, (offsetMap[u][v]-0.5) * offset);
				}
			}
		}
		//Has gain map
		else if(gainMap != null && offsetMap == null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					gainedMap[u][v] = gain(inputMap[u][v], preOffset, gainMap[u][v]*gain, offset);
				}
			}
		}
		//Has offset map
		else if(gainMap == null && offsetMap != null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					gainedMap[u][v] = gain(inputMap[u][v], preOffset, gain, (offsetMap[u][v]-0.5) * offset);
				}
			}
		}
		//Has only values and no map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					gainedMap[u][v] = gain(inputMap[u][v], preOffset, gain, offset);
				}
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					gainedMap[u][v] = gainedMap[u][v] * mask[u][v] + inputMap[u][v] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.setHeightmap(gainedMap);
		
		return requestData;
	}
	
	private float gain(float height, float preOffset, double gain, double offset) {
		height += preOffset;
		height *= gain;
		height -= preOffset;
		height += offset;
		height = Math.min(height, 1);
		height = Math.max(height, 0);
		return height;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("gain", new ModuleInputRequest(getInput(1), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("offset", new ModuleInputRequest(getInput(2), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("mask", new ModuleInputRequest(getInput(3), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Gain";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Gain"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Offset"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
