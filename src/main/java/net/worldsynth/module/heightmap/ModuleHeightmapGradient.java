/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.common.math.MathHelperScalar;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;

public class ModuleHeightmapGradient extends AbstractModule {

	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 100.0, 1.0, Double.MAX_VALUE, 1.0, 1000.0);
	private DoubleParameter direction = new DoubleParameter("direction", "Direction", null, 0.0, -360.0, 360.0, 0.0, 360.0);
	private DoubleParameter distortion = new DoubleParameter("distortion", "Distortion", null, 1.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 100.0);
	private EnumParameter<GradientTiling> tiling = new EnumParameter<GradientTiling>("tiling", "Tiling", null, GradientTiling.class, GradientTiling.NONE);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				scale,
				direction,
				tiling,
				distortion
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		double scale = this.scale.getValue();
		double direction = this.direction.getValue();
		double distortion = this.distortion.getValue();
		GradientTiling tiling = this.tiling.getValue();
		float[][] directionMap = null;
		float[][][] distortionMap = null;
		float[][] maskMap = null;
		
		//Read in scale
		if(inputs.get("scale") != null) {
			scale = ((DatatypeScalar) inputs.get("scale")).data;
		}
		
		//Read in direction
		if(inputs.get("direction") != null) {
			if(inputs.get("direction") instanceof DatatypeScalar) {
				direction = ((DatatypeScalar) inputs.get("direction")).data;
			}
			else {
				directionMap = ((DatatypeHeightmap) inputs.get("direction")).heightMap;
			}
		}
		
		//Read in distortion
		if(inputs.get("distortion") != null) {
			if(inputs.get("distortion") instanceof DatatypeVectormap) {
				distortionMap = ((DatatypeVectormap) inputs.get("distortion")).vectorField;
			}
			else {
				float[][] distortionHeightmap = ((DatatypeHeightmap) inputs.get("distortion")).getHeightmap();
				distortionMap = new float[mpw][mpl][2];
				for(int u = 0; u < mpw; u++) {
					for(int v = 0; v < mpl; v++) {
						distortionMap[u][v][0] = (float) Math.cos(distortionHeightmap[u][v] * Math.PI * 2.0);
						distortionMap[u][v][1] = (float) Math.sin(distortionHeightmap[u][v] * Math.PI * 2.0);
					}
				}
			}
		}
		
		//Read in mask
		if(inputs.get("mask") != null) {
			maskMap = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		//Has some input maps
		if(directionMap != null || distortionMap != null) {
			double xDistortion = 0.0;
			double zDistortion = 0.0;
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					if(directionMap != null) direction = directionMap[u][v] * 360.0f;
					if(distortionMap != null) {
						xDistortion = distortionMap[u][v][0];
						zDistortion = distortionMap[u][v][1];
					}
					
					float o = (float) getHeightAt(x+u*res+xDistortion*distortion, z+v*res+zDistortion*distortion, scale, direction, tiling);
					map[u][v] = MathHelperScalar.clamp(o, 0.0f, 1.0f);
				}
			}
		}
		//Has only values and no map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = (float) getHeightAt(x+u*res, z+v*res, scale, direction, tiling);
					map[u][v] = MathHelperScalar.clamp(o, 0.0f, 1.0f);
				}
			}
		}
		
		//Apply mask
		if(maskMap != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = map[u][v] * maskMap[u][v];
				}
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	public double getHeightAt(double x, double y, double scale, double direction, GradientTiling tiling) {
		double directionRadians = Math.toRadians(direction);
		if(direction != 0.0) {
			x = x * Math.cos(directionRadians) - y * Math.sin(directionRadians);
		}
		return gradient(x, scale, tiling);
	}
	
	private double gradient(double x, double scale, GradientTiling tiling) {
		double h = 0;
		h = x/scale;
		if(tiling == GradientTiling.NONE) {
			h = Math.min(Math.max(h, 0), 1);
		}
		else if(tiling == GradientTiling.TILING) {
			h -= Math.floor(h);
		}
		else if(tiling == GradientTiling.CONTINOUS) {
			h -= Math.floor(h);
			h *= 2;
			if(h > 1) {
				h = 2 - h;
			}
		}
		
		return h;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		inputRequests.put("scale", new ModuleInputRequest(getInput("Scale"), new DatatypeScalar()));
		inputRequests.put("direction", new ModuleInputRequest(getInput("Direction"), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		
		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		DatatypeVectormap requestVectorMap = new DatatypeVectormap(ord.x, ord.z, ord.width, ord.length, ord.resolution);
		inputRequests.put("distortion", new ModuleInputRequest(getInput("Distortion"), new DatatypeMultitype(new AbstractDatatype[] {(DatatypeHeightmap)outputRequest.data, requestVectorMap})));
		
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), (DatatypeHeightmap) outputRequest.data));
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Gradient";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Direction"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeHeightmap(), new DatatypeVectormap()}), "Distortion"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum GradientTiling {
		NONE, TILING, CONTINOUS;
	}
}
