/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;

public class ModuleHeightmapTranslate extends AbstractModule {
	
	private DoubleParameter xTranslate = new DoubleParameter("xtranslate", "Translate x", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -100.0, 100.0);
	private DoubleParameter zTranslate = new DoubleParameter("ztranslate", "Translate z", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -100.0, 100.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				xTranslate,
				zTranslate
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("input");
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		double xt = xTranslate.getValue();
		double zt = zTranslate.getValue();
		
		DatatypeScalar scalarDataX = (DatatypeScalar) buildInputData(new ModuleInputRequest(getInput(1), new DatatypeScalar()));
		DatatypeScalar scalarDataZ = (DatatypeScalar) buildInputData(new ModuleInputRequest(getInput(2), new DatatypeScalar()));
		
		if(scalarDataX != null) {
			xt = scalarDataX.data;
		}
		if(scalarDataZ != null) {
			zt = scalarDataZ.data;
		}
		
		DatatypeHeightmap requestData = (DatatypeHeightmap) outputRequest.data;
		DatatypeHeightmap translatedRequestData = new DatatypeHeightmap(requestData.x-xt, requestData.z-zt, requestData.width, requestData.length, requestData.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), translatedRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Translate";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "xTranslate"),
				new ModuleInput(new DatatypeScalar(), "zTranslate")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
