/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.LongParameter;

/**
 * Pseudo erosion based on the Iterative Pseudo-Erosion method decribed by /u/YankeeMinstrel on /r/proceduralgeneration
 */

public class ModuleHeightmapMacSearlasPseudoErosion extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 20.0, 1.0, Double.MAX_VALUE, 1.0, 100.0);
	private DoubleParameter intensity = new DoubleParameter("intensity", "Intensity", null, 1.0, 0.0, Double.POSITIVE_INFINITY, 0.0, 1.0);
	private DoubleParameter dispersion = new DoubleParameter("dispersion", "Dispersion", null, 1.0, 0.0, 1.0);
	
	private int permutationSize = 256;
	private int repeat = permutationSize;
	
	////////////////////////
	//This is for debuging//
	////////////////////////
	private boolean debugWindow = false;
	private boolean openDebugWindowOnFirstRun = debugWindow;
	
	/**
	 * This contains a double duplicated permutation table
	 */
	int[] dp = new int[permutationSize*2];
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newSeed -> {
			int[] p = createPermutatationTable(permutationSize, newSeed);
			for(int pi = 0; pi < dp.length; pi++) {
				dp[pi] = p[pi%permutationSize];
			}
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				intensity,
				dispersion,
				seed
				};
		return p;
	}
	
	private int[] createPermutatationTable(int size, long seed) {
		//Create a random generator with supplied seed
		Random r = new Random(seed);
		
		//Generate a list containing every integer from 0 inclusive to size exlusive
		ArrayList<Integer> valueTabel = new ArrayList<Integer>();
		for(int i = 0; i < size; i++) {
			valueTabel.add(i);
		}
		
		//create the permutation table
		int[] permutationTable = new int[size];
		
		//Insert the values from the valueTable into the permutation table in a random order
		int pi = 0;
		while(valueTabel.size() > 0) {
			int index = r.nextInt(valueTabel.size());
			permutationTable[pi] = valueTabel.get(index);
			valueTabel.remove(index);
			pi++;
		}
		
		return permutationTable;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		double scale = this.scale.getValue();
		DatatypeHeightmap d = (DatatypeHeightmap)outputRequest.data;
		DatatypeHeightmap inputRequestDatatype = new DatatypeHeightmap((float)(d.x-3*scale), (float)(d.z-3*scale), (float)(d.width+6*scale), (float)(d.length+6*scale), d.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput(0), inputRequestDatatype));
		
		inputRequests.put("mask", new ModuleInputRequest(getInput(1), (DatatypeHeightmap) outputRequest.data));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		double scale = this.scale.getValue();
		double intensity = this.intensity.getValue();
		double dispersion = this.dispersion.getValue();
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		DatatypeHeightmap input = (DatatypeHeightmap) inputs.get("input");
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		openDebugWindowOnFirstRun = debugWindow;
		
		//Has mask
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float erosion = (float) macSearlasAt(input, x+u*res, z+v*res, scale, dispersion);
				erosion *= intensity;
				if(mask != null) {
					erosion *= mask[u][v];
				}
				
				float height = 0;
				if(request.output.getName().equals("Erosionmap")) {
					height = erosion;
				}
				else {
					height = input.getGlobalHeight(x+u*res, z+v*res) - erosion;
				}
				height = Math.min(height, 1);
				height = Math.max(height, 0);
				
				map[u][v] = height;
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	private double macSearlasAt(DatatypeHeightmap heightdata, double globalx, double globaly, double scale, double dispersion) {
		double x = globalx / scale;
		double y = globaly / scale;
		
		//Calculate the coordinates inside the repeating area
		if(repeat > 0) {
			if(x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if(y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		
		//Generate the 25 closest points in global space
		double[] px = new double[25];
		double[] py = new double[25];
		float[] ph = new float[25];
		
		for(int ix = -2; ix < 3; ix++) {
			for(int iy = -2; iy < 3; iy++) {
				//The coordinates of the current unit square
				int cx = inc(xi, ix);
				int cy = inc(yi, iy);
				
				//The x and y hash for the unit square
				int xh = hashX(cx, cy);
				int yh = hashY(cx, cy);
				
				//The point offset in the unit square
				double xoffset = (double) xh/(double) repeat - 0.5;
				double yoffset = (double) yh/(double) repeat - 0.5;
				
				xoffset *= dispersion;
				yoffset *= dispersion;
				
				//Convert the point into global space
				double dx_punkt_og_koordinat = (double)ix + (0.5 + xoffset) - xf;
				double dy_punkt_og_koordinat = (double)iy + (0.5 + yoffset) - yf;
				
				double cpx = globalx + dx_punkt_og_koordinat * scale;
				double cpy = globaly + dy_punkt_og_koordinat * scale;
				
				px[(ix+2)+5*(iy+2)] = cpx;
				py[(ix+2)+5*(iy+2)] = cpy;
				
				//Get the height of the point
				ph[(ix+2)+5*(iy+2)] = heightdata.getGlobalHeight(cpx, cpy);
			}
		}
		
		//Connect points to lowest neighbour
		int[] pc = new int[25];
		for(int i = 0; i < 25; i++) {
			pc[i] = i;
		}
		for(int ix = 0; ix < 5; ix++) {
			for(int iy = 0; iy < 5; iy++) {
				
				double lowestHeight = 2;
				
				for(int sx = -1; sx < 2; sx++) {
					for(int sy = -1; sy < 2; sy++) {
						
						int cx = Math.max(0, Math.min(4, (ix+sx)));
						int cy = Math.max(0, Math.min(4, (iy+sy)));
						
						double h = ph[cx + 5*cy];
						
						if(h < lowestHeight) {
							lowestHeight = h;
							pc[ix + 5*iy] = cx+5*cy;
						}
						
					}
				}
				
				
			}
		}
		
		//Debug window
		if(openDebugWindowOnFirstRun) {
			JFrame debugFrame = new JFrame("MacSearlas Pseudo Erosion Debug");
			debugFrame.add(new DebugPanel(px, py, ph, pc));
			debugFrame.pack();
			debugFrame.setVisible(true);
			debugFrame.repaint();
			openDebugWindowOnFirstRun = false;
		}
		
		//Iterate over the closest points
		double height = 10000.0;
		for(int ix = 1; ix < 4; ix++) {
			for(int iy = 1; iy < 4; iy++) {
				double x1 = px[ix+5*iy];
				double y1 = py[ix+5*iy];
				
				double x2 = px[pc[ix+5*iy]];
				double y2 = py[pc[ix+5*iy]];
				
				double f1 = ((y1-y2)*(globaly-y1)+(x1-x2)*(globalx-x1))/(sqr(y1-y2)+sqr(x1-x2));
				double f2 = Math.abs(((y1-y2)*(globalx-x1)-(x1-x2)*(globaly-y1))/sqrt(sqr(x1-x2)+sqr(y1-y2)));
				
				double eh = 0;
				if(f1 > 0.0) {
					eh = sqrt(sqr(globalx-x1)+sqr(globaly-y1));
				}
				else if(f1 < -1.0) {
					eh = sqrt(sqr(globalx-x2)+sqr(globaly-y2));
				}
				else {
					eh = f2;
				}
				
				if(eh < height) {
					height = eh;
				}
			}
		}
		
		height /= scale * 2;
		
		return height;
	}
	
	private double sqrt(double a) {
		return Math.sqrt(a);
	}
	
	private double sqr(double a) {
		return a * a;
	}
	
	private int hashX(int x, int y) {
		int h = dp[dp[x]+y];
		return h;
	}
	private int hashY(int x, int y) {
		int h = dp[dp[y]+x];
		return h;
	}
	
	private int inc(int num, int n) {
		num += n;
		int ret;
		if(num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}

	@Override
	public String getModuleName() {
		return "MacSearlas pseudo erosion";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output"),
				new ModuleOutput(new DatatypeHeightmap(), "Erosionmap")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private class DebugPanel extends JPanel {
		private static final long serialVersionUID = 8862603853625476381L;
		
		double[] px;
		double[] py;
		float[] ph;
		int[] pc;
		
		double minx;
		double miny;
		double maxx;
		double maxy;
		
		double scale;
		
		public DebugPanel(double[] px, double[] py, float[] ph, int[] pc) {
			this.setPreferredSize(new Dimension(420, 420));
			
			this.px = px;
			this.py = py;
			this.ph = ph;
			this.pc = pc;
			
			for(int i = 0; i < 25; i++) {
				if(px[i] < minx) {
					minx = px[i];
				}
				if(py[i] < miny) {
					miny = py[i];
				}
				if(px[i] > maxx) {
					maxx = px[i];
				}
				if(py[i] > maxy) {
					maxy = py[i];
				}
				
				double dx = maxx - minx;
				double dy = maxy - miny;
				
				scale = dy;
				if(dx > dy) {
					scale = dx;
				}
			}
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getWidth(), getHeight());
			
			g.setColor(Color.WHITE);
			for(int i = 0; i < 25; i++) {
				g.setColor(new Color(ph[i], ph[i], ph[i]));
				g.fillOval((int)((px[i]-minx)/scale*400.0)-2+10, (int)((py[i]-miny)/scale*400.0)-2+10, 5, 5);
				g.drawLine((int)((px[i]-minx)/scale*400.0)-2+10, (int)((py[i]-miny)/scale*400.0)-2+10, (int)((px[pc[i]]-minx)/scale*400.0)-2+10, (int)((py[pc[i]]-miny)/scale*400.0)-2+10);
			}
		}
	}
}
