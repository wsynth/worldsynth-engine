/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;

public class ModuleHeightmapRamp extends AbstractModule {
	
	private EnumParameter<RampTiling> rampType = new EnumParameter<RampTiling>("tiling", "Ramp type", null, RampTiling.class, RampTiling.STANDARD);
	private DoubleParameter frequency = new DoubleParameter("frequency", "Ramp frequency", null, 1.0, 1.0, Double.MAX_VALUE, 1.0, 10.0);
	private BooleanParameter normalize = new BooleanParameter("normalize", "Normalize height", null, false);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				rampType,
				frequency,
				normalize
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		RampTiling rampType = this.rampType.getValue();
		boolean normalize = this.normalize.getValue();
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in frequency
		double frequency = this.frequency.getValue();
		if(inputs.get("frequency") != null) {
			frequency = ((DatatypeScalar) inputs.get("frequency")).data;
		}
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] outputMap = new float[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float o = ramp(inputMap[u][v], frequency, rampType, normalize);
				o = Math.min(o, 1);
				o = Math.max(o, 0);
				outputMap[u][v] = o;
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					outputMap[u][v] = outputMap[u][v] * mask[u][v] + inputMap[u][v] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.setHeightmap(outputMap);
		
		return requestData;
	}
	
	private float ramp(float height, double frequency, RampTiling method, boolean normalizeScale) {
		double period = 1.0/frequency;
		
		double h = height/period;
		double i = Math.floor(h);
		h = h-i;
		if(method == RampTiling.CONTINOUS && i%2 != 0) {
			h = 1-h;
		}
		if(!normalizeScale) {
			h *= period;
		}
		
		return (float) h;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("frequency", new ModuleInputRequest(getInput(1), new DatatypeScalar()));
		inputRequests.put("mask", new ModuleInputRequest(getInput(2), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Ramp";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "Frequency"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum RampTiling {
		STANDARD, CONTINOUS;
	}
}
