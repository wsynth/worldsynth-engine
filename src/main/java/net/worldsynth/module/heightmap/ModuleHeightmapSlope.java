/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;

public class ModuleHeightmapSlope extends AbstractModule {
	
	private FloatParameter gain = new FloatParameter("gain", "Gain", null, 1.0f, 0.0f, Float.POSITIVE_INFINITY, 0.0f, 20.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				gain
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		float res = (float) requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in gain
		double gainValue = this.gain.getValue();
		if(inputs.get("gain") != null) {
			gainValue = ((DatatypeScalar) inputs.get("gain")).data;
		}
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] outputMap = new float[mpw][mpl];
		
		for(int u = 0; u < outputMap.length; u++) {
			for(int v = 0; v < outputMap[u].length; v++) {
				float dx = (((inputMap[u+1][v+1] - inputMap[u][v+1]) + (inputMap[u+2][v+1] - inputMap[u+1][v+1]))/res)/2;
				float dy = (((inputMap[u+1][v+1] - inputMap[u+1][v]) + (inputMap[u+1][v+2] - inputMap[u+1][v+1]))/res)/2;
				float o = (float) (Math.sqrt(dx*dx + dy*dy) * gainValue);
				o = Math.min(o, 1);
				o = Math.max(o, 0);
				outputMap[u][v] = o;
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					outputMap[u][v] = outputMap[u][v] * mask[u][v] + inputMap[u+1][v+1] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.setHeightmap(outputMap);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap d = (DatatypeHeightmap)outputRequest.data;
		DatatypeHeightmap inputRequestDatatype = new DatatypeHeightmap(d.x-d.resolution, d.z-d.resolution, d.width+3.0*d.resolution, d.length+3.0*d.resolution, d.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Primary input"), inputRequestDatatype));
		
		inputRequests.put("gain", new ModuleInputRequest(getInput("Gain"), new DatatypeScalar()));
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Slope";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "Gain"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
