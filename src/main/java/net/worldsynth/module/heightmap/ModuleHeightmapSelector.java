/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import net.worldsynth.biome.Biome;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.table.BiomeColumn;
import net.worldsynth.parameter.table.StringColumn;
import net.worldsynth.parameter.table.TableParameter;

public class ModuleHeightmapSelector extends AbstractModule {
	
	private BiomeColumn<BiomeHeightmapInputEntry> biomeColumn = new BiomeColumn<BiomeHeightmapInputEntry>("biome", "Biome", null) {

		@Override
		public Biome getColumnValue(BiomeHeightmapInputEntry par) {
			return par.getBiome();
		}

		@Override
		public void setColumnValue(BiomeHeightmapInputEntry row, Biome value) {
			row.setBiome(value);
		}
	};
	
	private StringColumn<BiomeHeightmapInputEntry> heightmapInputColumn = new StringColumn<BiomeHeightmapInputEntry>("heightmapinput", "Heightmap input", null) {
		
		@Override
		public String getColumnValue(BiomeHeightmapInputEntry par) {
			return par.getHeightmapinput();
		}
		
		@Override
		public void setColumnValue(BiomeHeightmapInputEntry row, String value) {
			row.setHeightmapinput(value);
		}
	};
	
	private TableParameter<BiomeHeightmapInputEntry> mapping = new TableParameter<BiomeHeightmapInputEntry>("mapping", "Mapping", null,
			Arrays.asList(
					new BiomeHeightmapInputEntry(BiomeRegistry.getDefaultBiome(), BiomeRegistry.getDefaultBiome().getDisplayName())
					),
			biomeColumn, heightmapInputColumn) {

				@Override
				public BiomeHeightmapInputEntry newRow() {
					return new BiomeHeightmapInputEntry(Biome.NULL, Biome.NULL.getDisplayName());
				}
	};
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				mapping
		};
		return p;
	}
	
	@Override
	protected void onParametersChange() {
		reregisterIO();
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		if(inputs.get("inputselector") == null) {
			//If the biomemap input is null, there is not enough input and then just return null
			return null;
		}
		Biome[][] biomemap = ((DatatypeBiomemap) inputs.get("inputselector")).getBiomemap();
		
		if(inputs.get("default") == null) {
			//If the default heightmap input is null, there is not enough input and then just return null
			return null;
		}
		DatatypeHeightmap defautHeightmap = (DatatypeHeightmap) inputs.get("default");
		
		HashMap<Biome, DatatypeHeightmap> map = new HashMap<>();
		for(BiomeHeightmapInputEntry e: mapping.getValue()) {
			DatatypeHeightmap inputHeightmap = (DatatypeHeightmap) inputs.get(e.getHeightmapinput());
			if(inputHeightmap == null) {
				//If a heightmap input is null, there is not enough input and then just return null
				return null;
			}
			map.put(e.getBiome(), inputHeightmap);
		}
		
		//----------BUILD----------//
		
		float[][] heightmap = new float[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float o = 0.0f;
				Biome b = biomemap[u][v];
				
				o = map.getOrDefault(b, defautHeightmap).getLocalHeight(u, v);
				
				o = Math.min(o, 1);
				o = Math.max(o, 0);
				heightmap[u][v] = o;
			}
		}
		
		requestData.setHeightmap(heightmap);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap outputRequestData = (DatatypeHeightmap) outputRequest.data;
		
		DatatypeBiomemap biomemapInputRequestData = new DatatypeBiomemap(outputRequestData.x, outputRequestData.z, outputRequestData.width, outputRequestData.length, outputRequestData.resolution);
		ModuleInputRequest biomemapInputRequest = new ModuleInputRequest(getInput("Selector input"), biomemapInputRequestData);
		
		inputRequests.put("inputselector", biomemapInputRequest);
		inputRequests.put("default", new ModuleInputRequest(getInput("Default"), outputRequest.data));
		for(BiomeHeightmapInputEntry e: mapping.getValue()) {
			inputRequests.put(e.getHeightmapinput(), new ModuleInputRequest(getInput(e.getHeightmapinput()), outputRequest.data));
		}
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Selector";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		LinkedHashMap<String, ModuleInput> in = new LinkedHashMap<>();
		in.put("Selector input", new ModuleInput(new DatatypeBiomemap(), "Selector input"));
		in.put("Default", new ModuleInput(new DatatypeHeightmap(), "Default"));
		for(BiomeHeightmapInputEntry e: mapping.getValue()) {
			if(!in.containsKey(e.getHeightmapinput())) {
				in.put(e.getHeightmapinput(), new ModuleInput(new DatatypeHeightmap(), e.getHeightmapinput()));
			}
		}
		return in.values().toArray(new ModuleInput[in.size()]);
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private static class BiomeHeightmapInputEntry {
		private Biome biome;
		private String heightmapInput;
		
		public BiomeHeightmapInputEntry(Biome biome, String heightmapInput) {
			this.heightmapInput = heightmapInput;
			this.biome = biome;
		}
		
		public final Biome getBiome() {
			return biome;
		}
		
		public final void setBiome(Biome biome) {
			this.biome = biome;
		}
		
		public final String getHeightmapinput() {
			return heightmapInput;
		}
		
		public final void setHeightmapinput(String heightmapInput) {
			this.heightmapInput = heightmapInput;
		}
		
		@Override
		protected BiomeHeightmapInputEntry clone() {
			return new BiomeHeightmapInputEntry(getBiome(), getHeightmapinput());
		}
	}
}
