package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.common.math.MathHelperScalar;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.IntegerParameter;

public class ModuleHeightmapResample extends AbstractModule {
	
	private IntegerParameter resamples = new IntegerParameter("resamples", "Resamples", null, 8, 1, Integer.MAX_VALUE, 1, 32);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				resamples
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		DatatypeHeightmap i1 = (DatatypeHeightmap) inputs.get("input");
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				map[u][v] = i1.getGlobalLerpHeight(x + u*res, z + v*res);
				map[u][v] = MathHelperScalar.clamp(map[u][v], 0.0f, 1.0f);
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	private double[] getSampleExtent(double x, double z, double width, double length, double sampleGridSize) {
		double x1 = Math.floor(x / sampleGridSize);
		double z1 = Math.floor(z / sampleGridSize);

		double x2 = Math.ceil((x + width) / sampleGridSize) + 1.0;
		double z2 = Math.ceil((z + length) / sampleGridSize) + 1.0;

		return new double[] {
				x1 * sampleGridSize,
				z1 * sampleGridSize,
				x2 * sampleGridSize,
				z2 * sampleGridSize};
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		double res = Math.max(resamples.getValue(), ord.resolution);
		double[] reg = getSampleExtent(ord.x, ord.z, ord.width, ord.length, resamples.getValue());
		double x = reg[0];
		double z = reg[1];
		double width = reg[2] - reg[0];
		double length = reg[3] - reg[1];
		
		DatatypeHeightmap inputHeightmapRequestData = new DatatypeHeightmap(x, z, width, length, res);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputHeightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Resample";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
