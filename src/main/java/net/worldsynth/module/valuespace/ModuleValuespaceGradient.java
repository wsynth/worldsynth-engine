/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;

public class ModuleValuespaceGradient extends AbstractModule {
	
	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 100.0, 0.0, Double.MAX_VALUE, 1.0, 100.0);
	private DoubleParameter direction = new DoubleParameter("rotation", "Direction", null, 0.0, -360.0, 360.0, 0.0, 360.0);
	private DoubleParameter tilt = new DoubleParameter("tilt", "Tilt", null, 0.0, -90, 90, 0.0, 90.0);
	private EnumParameter<GradientTiling> tiling = new EnumParameter<GradientTiling>("tiling", "Tiling", null, GradientTiling.class, GradientTiling.NONE);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				scale,
				direction,
				tilt,
				tiling
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.x;
		double y = requestData.y;
		double z = requestData.z;
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		double scale = this.scale.getValue();
		double direction = this.direction.getValue();
		double tilt = this.tilt.getValue();
		GradientTiling tiling = this.tiling.getValue();
		
		boolean useHeightmap = false;
		float[][] inputMap0 = null;
		if(inputs.get("dmap") != null) {
			useHeightmap = true;
			inputMap0 = ((DatatypeHeightmap) inputs.get("dmap")).getHeightmap();
		}
		
		boolean useValuespace = false;
		float[][][] inputSpace1 = null;
		if(inputs.get("dspace") != null) {
			useValuespace = true;
			inputSpace1 = ((DatatypeValuespace) inputs.get("dspace")).valuespace;
		}

		float[][][] space = new float[spw][sph][spl];
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					float offset = 0;
					if(useHeightmap) {
						offset += inputMap0[u][w];
					}
					if(useValuespace) {
						offset += inputSpace1[u][v][w];
					}
					
					float o = (float) getValueAt(x+u*res, y+v*res, z+w*res, scale, offset, direction, tilt, tiling);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					space[u][v][w] = o;
				}
			}
		}
		
		requestData.valuespace = space;
		
		return requestData;
	}
	
	public double getValueAt(double x, double y, double z, double scale, double offset, double direction, double tilt, GradientTiling tiling) {
		double rr = Math.toRadians(-direction);
		double tr = Math.toRadians(tilt);
		
		//Rotate around y
		if(direction != 0.0) {
			double tx = x*cos(rr) + 0 + z*sin(rr);
			double ty = 0 + y - 0;
			//double tz = -x*sin(rr) + 0 + z*cos(rr);
			
			x = tx;
			y = ty;
			//z = tz;
		}
		//Tilt around z
		if(tilt != 0.0) {
			//double tx = x*cos(tr) - y*sin(tr) + 0;
			double ty = x*sin(tr) + y*cos(tr) + 0;
			//double tz = 0 + 0 + z;
			
			//x = tx;
			y = ty;
			//z = tz;
		}
		
		return gradient(y + offset*10.0, scale, tiling);
	}
	
	private double cos(double a) {
		return Math.cos(a);
	}
	
	private double sin(double a) {
		return Math.sin(a);
	}
	
	private double gradient(double x, double scale, GradientTiling tiling) {
		double h = 0;
		h = x/scale;
		if(tiling == GradientTiling.NONE) {
			h = Math.min(Math.max(h, 0), 1);
		}
		else if(tiling == GradientTiling.TILING) {
			h -= Math.floor(h);
		}
		else if(tiling == GradientTiling.CONTINOUS) {
			h -= Math.floor(h);
			h *= 2;
			if(h > 1) {
				h = 2 - h;
			}
		}
		
		return h;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeValuespace valuespaceRequestData = (DatatypeValuespace) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(valuespaceRequestData.x, valuespaceRequestData.z, valuespaceRequestData.width, valuespaceRequestData.length, valuespaceRequestData.resolution);
		
		inputRequests.put("dmap", new ModuleInputRequest(getInput(0), heightmapRequestData));
		inputRequests.put("dspace", new ModuleInputRequest(getInput(1), valuespaceRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Gradient";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_VALUESPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Displace map"),
				new ModuleInput(new DatatypeValuespace(), "Displace space")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	private enum GradientTiling {
		NONE, TILING, CONTINOUS;
	}
}
