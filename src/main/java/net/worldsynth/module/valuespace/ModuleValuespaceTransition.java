/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;

public class ModuleValuespaceTransition extends AbstractModule {
	
	private DoubleParameter transition = new DoubleParameter("transition", "Transition", null, 0.1, 0.0, 1.0, 256.0);
	private EnumParameter<Direction> direction = new EnumParameter<Direction>("direction", "Direction", null, Direction.class, Direction.UP);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				transition,
				direction
		};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		double res = requestData.resolution;
		
		//----------READ INPUTS----------//
		
		//Read in input
		if(inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//----------BUILD----------//
		
		float[][][] valuespace = new float[spw][sph][spl];
		
		double t = transition.getValue();
		Direction d = direction.getValue();
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					double o = (double) v/255.0 * res;
					if(d == Direction.UP) {
						o = (inputMap[u][w] - o + t) / t;
					}
					else {
						o = (o - inputMap[u][w] + t) / t;
					}
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					valuespace[u][v][w] = (float) o;
				}
			}
		}
		
		requestData.valuespace = valuespace;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeValuespace vbrd = (DatatypeValuespace) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(vbrd.x, vbrd.z, vbrd.width, vbrd.length, vbrd.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Transition";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_VALUESPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum Direction {
		UP, DOWN;
	}
}
