package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.common.math.MathHelperScalar;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.IntegerParameter;

public class ModuleValuespaceResample extends AbstractModule {
	
private IntegerParameter resamples = new IntegerParameter("resamples", "Resamples", null, 8, 1, Integer.MAX_VALUE, 1, 32);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				resamples
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.x;
		double y = requestData.y;
		double z = requestData.z;
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		DatatypeValuespace i1 = (DatatypeValuespace) inputs.get("input");
		
		//----------BUILD----------//
		
		float[][][] values = new float[spw][sph][spl];
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					values[u][v][w] = i1.getGlobalLerpValue(x + u*res, y + v*res, z + w*res);
					values[u][v][w] = MathHelperScalar.clamp(values[u][v][w], 0.0f, 1.0f);
				}
			}
		}
		
		requestData.setValuespace(values);
		
		return requestData;
	}
	
	private double[] getSampleExtent(double x, double y, double z, double width, double height, double length, double sampleGridSize) {
		double x1 = Math.floor(x / sampleGridSize);
		double y1 = Math.floor(y / sampleGridSize);
		double z1 = Math.floor(z / sampleGridSize);

		double x2 = Math.ceil((x + width) / sampleGridSize) + 1.0;
		double y2 = Math.ceil((y + height) / sampleGridSize) + 1.0;
		double z2 = Math.ceil((z + length) / sampleGridSize) + 1.0;

		return new double[] {
				x1 * sampleGridSize,
				y1 * sampleGridSize,
				z1 * sampleGridSize,
				x2 * sampleGridSize,
				y2 * sampleGridSize,
				z2 * sampleGridSize};
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeValuespace ord = (DatatypeValuespace) outputRequest.data;
		double res = Math.max(resamples.getValue(), ord.resolution);
		double[] reg = getSampleExtent(ord.x, ord.y, ord.z, ord.width, ord.height, ord.length, resamples.getValue());
		double x = reg[0];
		double y = reg[1];
		double z = reg[2];
		double width = reg[3] - reg[0];
		double height = reg[4] - reg[1];
		double length = reg[5] - reg[2];
		
		DatatypeValuespace ird = new DatatypeValuespace(x, y, z, width, height, length, res);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), ird));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Resample";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_VALUESPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
