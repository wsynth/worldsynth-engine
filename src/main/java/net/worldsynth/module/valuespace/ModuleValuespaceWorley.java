/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.common.math.Permutation;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.LongParameter;

public class ModuleValuespaceWorley extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 100.0, 1.0, Double.MAX_VALUE, 1.0, 1000.0);
	private DoubleParameter amplitude = new DoubleParameter("amplitude", "Amplitude", null, 1.0, 0.0, Double.MAX_VALUE, 0.0, 5.0);
	private DoubleParameter offset = new DoubleParameter("offset", "Offset", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1.0, 1.0, 256.0);
	private EnumParameter<FeatureDistrubution> featureDistrubution = new EnumParameter<FeatureDistrubution>("distrubution", "Cell type", null, FeatureDistrubution.class, FeatureDistrubution.VORONOI);
	private EnumParameter<DistanceFunction> distanceFunction = new EnumParameter<DistanceFunction>("distancefunction", "Distance function", null, DistanceFunction.class, DistanceFunction.EUCLIDEAN);
	private EnumParameter<Feature> feature = new EnumParameter<Feature>("feature", "Feature function", null, Feature.class, Feature.F1);
	
	private final int permutationSize = 256;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, permutationSize, 3);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				amplitude,
				offset,
				distanceFunction,
				feature,
				featureDistrubution,
				seed
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.x;
		double y = requestData.y;
		double z = requestData.z;
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		double scale = this.scale.getValue();
		double amplitude = this.amplitude.getValue();
		double offset = this.offset.getValue();
		FeatureDistrubution featureDistrubution = this.featureDistrubution.getValue();
		DistanceFunction distanceFunction = this.distanceFunction.getValue();
		Feature feature = this.feature.getValue();
		
		float[][][] values = new float[spw][sph][spl];
		
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					values[u][v][w] = (float) getValueAt(x+u*res, y+v*res, z+w*res, scale, amplitude, offset, featureDistrubution, distanceFunction, feature);
				}
			}
		}
		
		requestData.valuespace = values;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		return inputRequests;
	}
	
	public double getValueAt(double x, double y, double z, double scale, double amplitude, double offset, FeatureDistrubution featureDistrubution, DistanceFunction distanceFunction, Feature feature) {
		return worley3d(x/scale, y/scale, z/scale, amplitude, offset, featureDistrubution, distanceFunction, feature);
	}
	
	private double worley3d(double x, double y, double z, double amplitude, double offset, FeatureDistrubution featureDistrubution, DistanceFunction distanceFunction, Feature feature) {
		if(repeat > 0) {
			if(x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if(y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
			if(z < 0) {
				z = repeat+(z%repeat);
			}
			else {
				z = z%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		int zi = (int)z & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		double zf = z - (int)z;
		
		//Calculate the local coordinates of the 9 closest featurepoints
		double[][][] fx = new double[3][3][3];
		double[][][] fy = new double[3][3][3];
		double[][][] fz = new double[3][3][3];
		
		double[] dist = new double[27];
		for(int i = 0; i < 27; i++) {
			dist[i] = -1;
		}
		
		for(int ix = 0; ix < 3; ix++) {
			for(int iy = 0; iy < 3; iy++) {
				for(int iz = 0; iz < 3; iz++) {
					int cx = inc(xi, ix-1);
					int cy = inc(yi, iy-1);
					int cz = inc(zi, iz-1);
					
					int xh = permutation.lHash(0, cx, cy, cz);
					int yh = permutation.lHash(1, cx, cy, cz);
					int zh = permutation.lHash(2, cx, cy, cz);
					
					double xoffset = 0.5;
					double yoffset = 0.5;
					double zoffset = 0.5;
					
					if(featureDistrubution == FeatureDistrubution.VORONOI) {
						xoffset = (double) xh/(double) repeat;
						yoffset = (double) yh/(double) repeat;
						zoffset = (double) zh/(double) repeat;
					}
					
					fx[ix][iy][iz] = (double) (ix-1) + xoffset;
					fy[ix][iy][iz] = (double) (iy-1) + yoffset;
					fz[ix][iy][iz] = (double) (iz-1) + zoffset;
					
					double xdist = fx[ix][iy][iz] - xf;
					double ydist = fy[ix][iy][iz] - yf;
					double zdist = fz[ix][iy][iz] - zf;
					
					//Distance from point
					switch (distanceFunction) {
					case EUCLIDEAN:
						dist[ix*9 + iy*3 + iz] = Math.sqrt(xdist*xdist + ydist*ydist + zdist*zdist);
						break;
					case MANHATTAN:
						dist[ix*9 + iy*3 + iz] = Math.abs(xdist) + Math.abs(ydist) + Math.abs(zdist);
						break;
					case EUCLIDEAN_SQUARED:
						dist[ix*9 + iy*3 + iz] = xdist*xdist + ydist*ydist + zdist*zdist;
						break;
					case CHEBYSHEV:
						dist[ix*9 + iy*3 + iz] = Math.max(Math.max(Math.abs(xdist), Math.abs(ydist)), Math.abs(zdist));
						break;
					case MIN:
						dist[ix*9 + iy*3 + iz] = Math.min(Math.min(Math.abs(xdist), Math.abs(ydist)), Math.abs(zdist));
						break;
					}
				}
			}
		}
		
		//Sort
		for(int i = 0; i < feature.getMaxFeature(); i++) {
			for(int j = 26; j > i; j--) {
				if(dist[j] < dist[j-1]) {
					double temp = dist[j];
					dist[j] = dist[j-1];
					dist[j-1] = temp;
				}
			}
		}
		
		double height = 0;
		switch (feature) {
		case F1:
			height = dist[0];
			break;
		case F2:
			height = dist[1];
			break;
		case F3:
			height = dist[2];
			break;
		case F2_F1:
			height = dist[1] - dist[0];
			break;
		case F3_F1:
			height = dist[2] - dist[0];
			break;
		case F3_F2:
			height = dist[2] - dist[1];
			break;
		}
		
		height *= amplitude;
		height += offset;
		height = Math.min(height, 1);
		height = Math.max(height, 0);
		return height;
	}
	
	private int inc(int num, int n) {
		num += n;
		int ret;
		if(num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}

	@Override
	public String getModuleName() {
		return "Valuespace worley";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_VALUESPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum DistanceFunction {
		EUCLIDEAN,
		MANHATTAN,
		EUCLIDEAN_SQUARED,
		CHEBYSHEV,
		MIN;
	}
	
	private enum Feature {
		F1(1),
		F2(2),
		F3(3),
		F2_F1(2),
		F3_F1(3),
		F3_F2(3);
		
		private final int maxFeature;
		
		private Feature(int maxFeature) {
			this.maxFeature = maxFeature;
		}
		
		int getMaxFeature() {
			return maxFeature;
		}
	}
	
	private enum FeatureDistrubution {
		VORONOI,
		SQUARE;
	}
}
