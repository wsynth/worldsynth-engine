/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;

public class ModuleValuespacePointDistance extends AbstractModule {
	
	public DoubleParameter distance = new DoubleParameter("dist", "Distance", null, 50, 0.0, Double.MAX_VALUE, 0.0, 200.0);
	public DoubleParameter pointX = new DoubleParameter("pointx", "Point X", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1000, 100.0);
	public DoubleParameter pointY = new DoubleParameter("pointy", "Point Y", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1000, 100.0);
	public DoubleParameter pointZ = new DoubleParameter("pointz", "Point Z", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1000, 100.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				distance,
				pointX,
				pointY,
				pointZ
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.x;
		double y = requestData.y;
		double z = requestData.z;
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		double distance = this.distance.getValue();
		double pointX = this.pointX.getValue();
		double pointY = this.pointY.getValue();
		double pointZ = this.pointZ.getValue();
		
		float[][][] valuespace = new float[spw][sph][spl];
		
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					float o = 0.0f;
					
					double gx = x + (double)u * res;
					double gy = y + (double)v * res;;
					double gz = z + (double)w * res;;
					
					o = (float) (dist(pointX, pointY, pointZ, gx, gy, gz) / distance);
					
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					valuespace[u][v][w] = o;
				}
			}
		}
		
		requestData.valuespace = valuespace;
		
		return requestData;
	}
	
	private double dist(double x1, double y1, double z1, double x2, double y2, double z2) {
		double dx = x1 - x2;
		double dy = y1 - y2;
		double dz = z1 - z2;
		
		double d = Math.sqrt(dx * dx + dy * dy + dz * dz);
		
		return d;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Valuespace pointdistance";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_VALUESPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
