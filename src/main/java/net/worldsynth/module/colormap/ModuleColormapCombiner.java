/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.colormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.EnumParameter;

public class ModuleColormapCombiner extends AbstractModule {
	
	private EnumParameter<Operation> operation = new EnumParameter<Operation>("operation", "Arithmetic operation", null, Operation.class, Operation.ADDITION);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				operation
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		Operation operation = this.operation.getValue();
		
		//Check if both inputs are available
		if(inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If the primary or secondary input is null, there is not enough input and then just return null
			return null;
		}
		//Read in the primary and secondary colormaps
		float[][][] primaryMap = ((DatatypeColormap) inputs.get("primary")).colorMap;
		float[][][] secondaryMap = ((DatatypeColormap) inputs.get("secondary")).colorMap;
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
				
		//----------BUILD----------//
		
		float[][][] map = new float[mpw][mpl][];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float[] i0 = primaryMap[u][v];
				float[] i1 = secondaryMap[u][v];
				
				float[] o = {0.0f, 0.0f, 0.0f};
				
				for(int i = 0; i < 3; i++) {
					switch (operation) {
					case ADDITION:
						o[i] = i0[i] + i1[i];
						break;
					case SUBTRACTION:
						o[i] = i0[i] - i1[i];
						break;
					case MULTIPLICATION:
						o[i] = i0[i] * i1[i];
						break;
					case DIVISION:
						o[i] = i0[i] / i1[i];
						break;
					case AVERAGE:
						o[i] = (i0[i] + i1[i]) / 2;
						break;
					case MAX:
						o[i] = Math.max(i0[i], i1[i]);
						break;
					case MIN:
						o[i] = Math.min(i0[i], i1[i]);
						break;
					}
					
					o[i] = Math.min(o[i], 1);
					o[i] = Math.max(o[i], 0);
				}
				
				map[u][v] = o;
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					for(int i = 0; i < 3; i++) {
						map[u][v][i] = map[u][v][i] * mask[u][v] + primaryMap[u][v][i] * (1-mask[u][v]);
					}
				}
			}
		}
		
		requestData.colorMap = map;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput(1), outputRequest.data));
		
		DatatypeColormap colormapRequestData = (DatatypeColormap) outputRequest.data;
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(colormapRequestData.x, colormapRequestData.z, colormapRequestData.width, colormapRequestData.length, colormapRequestData.resolution);
		inputRequests.put("mask", new ModuleInputRequest(getInput(2), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Combiner";
	}
	
	@Override
	public String getModuleMetaTag() {
		return operation.getValue().name().substring(0, 3);
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER_COLORMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeColormap(), "Primary input"),
				new ModuleInput(new DatatypeColormap(), "Secondary input"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeColormap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum Operation {
		ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, AVERAGE, MAX, MIN;
	}
}
