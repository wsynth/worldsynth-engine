/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.colormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.common.color.WsColor;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.ColorParameter;
import net.worldsynth.synth.io.Element;

public class ModuleColormapConstant extends AbstractModule {
	
	private ColorParameter color = new ColorParameter("color", "Color", null, WsColor.MAGENTA);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				color
		};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		WsColor color = this.color.getValue();
		
		float[][][] map = new float[mpw][mpl][3];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				map[u][v][0] = color.getRed();
				map[u][v][1] = color.getGreen();
				map[u][v][2] = color.getBlue();
			}
		}
		
		requestData.colorMap = map;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Constant";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_COLORMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public void fromElement(Element element) {
		float red = 0.0f;
		float green = 0.0f;
		float blue = 0.0f;
		for(Element e: element.elements) {
			if(e.tag.equals(color.getName())) {
				color.fromElement(e);
				return;
			}
			else if(e.tag.equals("red")) {
				red = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("green")) {
				green = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("blue")) {
				blue = Float.parseFloat(e.content);
			}
		}
		color.setValue(new WsColor(red, green, blue));
	}
}
