/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.FloatParameter;

public class ModuleBlockspaceSurfaceCover extends AbstractModule {
	
	private BooleanParameter coverOnTop = new BooleanParameter("ontop", "Cover on top", null, false);
	private FloatParameter depth = new FloatParameter("depth", "Cover depth", null, 1.0f, 0.0f,  Float.MAX_VALUE, 0.0f, 100.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				depth,
				coverOnTop
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		float depth = this.depth.getValue();
		
		if(inputs.get("input") == null || inputs.get("material") == null) {
			return null;
		}
		
		MaterialState<?, ?>[][][] blockspace = ((DatatypeBlockspace) inputs.get("input")).getBlockspace();
		MaterialState<?, ?>[][] inputMaterialmap1 = ((DatatypeMaterialmap) inputs.get("material")).getMaterialmap();
		
		float[][] inputHeightmap2 = null;
		if(inputs.get("depth") != null) {
			inputHeightmap2 = ((DatatypeHeightmap) inputs.get("depth")).getHeightmap();
		}
		
		//Apply cover on top of existing blocks
		if(coverOnTop.getValue()) {
			for(int u = 0; u < spw; u++) {
				for(int w = 0; w < spl; w++) {
					
					double remainingDepth = depth;
					if(inputHeightmap2 != null) {
						remainingDepth *= inputHeightmap2[u][w];
					}
					
					int v = sph;
					while(--v >= 0) {
						if(!MaterialRegistry.isAir(blockspace[u][v][w])) {
							break;
						}
					}
					if(v < 0) {
						continue;
					}
					while(++v < sph && remainingDepth > 0) {
						blockspace[u][v][w] = inputMaterialmap1[u][w];
						remainingDepth -= res;
					}
				}
			}
		}
		//Replace existing blocks from the top down
		else {
			for(int u = 0; u < spw; u++) {
				for(int w = 0; w < spl; w++) {
					
					double remainingDepth = depth;
					if(inputHeightmap2 != null) {
						remainingDepth *= inputHeightmap2[u][w];
					}
					
					int v = sph;
					while(--v >= 0) {
						if(!MaterialRegistry.isAir(blockspace[u][v][w])) {
							break;
						}
					}
					if(v < 0) {
						continue;
					}
					v++;
					while(--v >= 0 && remainingDepth > 0) {
						blockspace[u][v][w] = inputMaterialmap1[u][w];
						remainingDepth -= res;
					}
				}
			}
		}
		
		
		requestData.setBlockspace(blockspace);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace outputRequestData = (DatatypeBlockspace) outputRequest.data;
		
		DatatypeMaterialmap materialmapRequeatData = new DatatypeMaterialmap(outputRequestData.x, outputRequestData.z, outputRequestData.width, outputRequestData.length, outputRequestData.resolution);
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(outputRequestData.x, outputRequestData.z, outputRequestData.width, outputRequestData.length, outputRequestData.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequestData));
		inputRequests.put("material", new ModuleInputRequest(getInput(1), materialmapRequeatData));
		inputRequests.put("depth", new ModuleInputRequest(getInput(2), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Surface cover";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Blockspace"),
				new ModuleInput(new DatatypeMaterialmap(), "Material"),
				new ModuleInput(new DatatypeHeightmap(), "Depth")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
