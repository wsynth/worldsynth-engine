/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;
import net.worldsynth.parameter.MaterialStateParameter;

public class ModuleBlockspaceFromValuespace extends AbstractModule {

	private FloatParameter highSelect = new FloatParameter("highselect", "High select", null, 1.0f, 0.0f, 1.0f);
	private FloatParameter lowSelect = new FloatParameter("lowselect", "Low select", null, 0.0f, 0.0f, 1.0f);
	private MaterialStateParameter material = new MaterialStateParameter("material", "Material", null, MaterialRegistry.getDefaultMaterial().getDefaultState());
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				highSelect,
				lowSelect,
				material
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		float highSelect = this.highSelect.getValue();
		float lowSelect = this.lowSelect.getValue();
		
		if(inputs.get("valuespace") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][][] inputSpace0 = ((DatatypeValuespace) inputs.get("valuespace")).valuespace;
		
		if(inputs.get("high") != null) {
			highSelect = (float) ((DatatypeScalar) inputs.get("high")).data;
		}
		
		if(inputs.get("low") != null) {
			lowSelect = (float) ((DatatypeScalar) inputs.get("low")).data;
		}
		
		MaterialState<?, ?>[][][] blockspace = new MaterialState<?, ?>[spw][sph][spl];
		
		MaterialState<?, ?> air = MaterialRegistry.getDefaultAir().getDefaultState();
		MaterialState<?, ?> material = this.material.getValue();
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					float i0 = inputSpace0[u][v][w];
					
					MaterialState<?, ?> o = air;
					if(i0 <= highSelect && i0 >= lowSelect) {
						o = material;
					}
					
					blockspace[u][v][w] = o;
				}
			}
		}
		
		requestData.setBlockspace(blockspace);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace dvb = (DatatypeBlockspace) outputRequest.data;
		
		DatatypeValuespace dvv = new DatatypeValuespace(dvb.x, dvb.y, dvb.z, dvb.width, dvb.height, dvb.length, dvb.resolution);
		
		inputRequests.put("valuespace", new ModuleInputRequest(getInput(0), dvv));
		inputRequests.put("high", new ModuleInputRequest(getInput(1), new DatatypeScalar()));
		inputRequests.put("low", new ModuleInputRequest(getInput(2), new DatatypeScalar()));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Blockspace from valuespace";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "High select"),
				new ModuleInput(new DatatypeScalar(), "Low select")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
