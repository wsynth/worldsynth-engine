/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.MaterialStateParameter;

public class ModuleBlockspaceFromHeightmap extends AbstractModule {
	
	private MaterialStateParameter material = new MaterialStateParameter("material", "Material", null, MaterialRegistry.getDefaultMaterial().getDefaultState());
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				material
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double y = requestData.y;
		double height = requestData.height;
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		MaterialState<?, ?>[][][] blockspace = new MaterialState<?, ?>[spw][sph][spl];
		
		float[][] inputMap0 = null;
		float[][] inputMap1 = null;
		
		if(inputs.get("high") != null) {
			inputMap0 = ((DatatypeHeightmap) inputs.get("high")).getHeightmap();
		}
		if(inputs.get("low") != null) {
			inputMap1 = ((DatatypeHeightmap) inputs.get("low")).getHeightmap();
		}
		
		float fy = (float)y/255.0f;
		float fheight = (float)height/255.0f;
		
		MaterialState<?, ?> material = this.material.getValue();
		
		for(int u = 0; u < spw; u++) {
			for(int w = 0; w < spl; w++) {
				
				float instantMinHeight = Math.max(fy, Integer.MIN_VALUE);
				float instantMaxHeight = Math.min(fy+fheight, Integer.MAX_VALUE);
				if(inputMap0 != null) {
					instantMaxHeight = Math.min(instantMaxHeight, inputMap0[u][w]);
				}
				if(inputMap1 != null) {
					instantMinHeight = Math.max(instantMinHeight, inputMap1[u][w]);
				}
				
				int minArrayHeight = (int)((instantMinHeight-fy)*255.0f/res);
				int maxArrayHeight = (int)((instantMaxHeight-fy)*255.0f/res);
				
				for(int v = minArrayHeight; v < maxArrayHeight; v++) {
					blockspace[u][v][w] = material;
				}
			}
		}
		
		requestData.setBlockspace(blockspace);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace vbrd = (DatatypeBlockspace) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(vbrd.x, vbrd.z, vbrd.width, vbrd.length, vbrd.resolution);
		
		inputRequests.put("high", new ModuleInputRequest(getInput(0), heightmapRequestData));
		inputRequests.put("low", new ModuleInputRequest(getInput(1), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Blockspace from heightmap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "High clamp"),
				new ModuleInput(new DatatypeHeightmap(), "Low clamp")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
