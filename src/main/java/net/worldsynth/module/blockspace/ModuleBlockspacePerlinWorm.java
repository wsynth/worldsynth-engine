/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.common.math.Permutation;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.LongParameter;
import net.worldsynth.parameter.MaterialStateParameter;

public class ModuleBlockspacePerlinWorm extends AbstractModule {
	
	private DoubleParameter wormLenght = new DoubleParameter("wormlenght", "Worms lenght", null, 20.0, 0.0, Double.MAX_VALUE, 0.0, 100.0);
	private DoubleParameter distribution = new DoubleParameter("distribution", "Distribution", null, 32.0, 1.0, Double.MAX_VALUE, 4.0, 100.0);
	private DoubleParameter distributionRandom = new DoubleParameter("distributionrandom", "Distribution random", null, 1.0, 0.0, 1.0);
	private DoubleParameter wormRadius = new DoubleParameter("wormradius", "Worm radius", null, 3.0, 1.0, Double.MAX_VALUE, 1.0, 10.0);
	private DoubleParameter noiseScale = new DoubleParameter("noisescale", "Noise scale", null, 50.0, 0.0, Float.MAX_VALUE, 0.0, 100.0);
	private MaterialStateParameter material = new MaterialStateParameter("material", "Material", null, MaterialRegistry.getDefaultMaterial().getDefaultState());
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	
	private PerlinGenerator[] perlinGenerators;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			Random r = new Random(newValue);
			perlinGenerators = new PerlinGenerator[] {
					new PerlinGenerator(r.nextLong()),
					new PerlinGenerator(r.nextLong()),
					new PerlinGenerator(r.nextLong())
			};
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				wormLenght,
				distribution,
				distributionRandom,
				wormRadius,
				noiseScale,
				material,
				seed
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		double wormLenght = this.wormLenght.getValue();
		double wormRadius = this.wormRadius.getValue();
		double distribution = this.distribution.getValue();
		double distributionRandom = this.distributionRandom.getValue();
		double noiseScale = this.noiseScale.getValue();
		
		requestData.setBlockspace(new MaterialState<?, ?>[spw][sph][spl]);
		
		ArrayList<ArrayList<BlockPosition>> worms = new ArrayList<ArrayList<BlockPosition>>();
		
		for(BlockPosition bp: getStartingPoints(requestData, wormLenght, distribution, distributionRandom, seed.getValue())) {
			worms.add(generateWorm(bp.x, bp.y, bp.z, wormLenght, noiseScale));
		}
		
		MaterialState<?, ?> material = this.material.getValue();
		for(ArrayList<BlockPosition> worm: worms) {
			digWorm(worm, requestData, wormRadius, material);
		}
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> ir = new HashMap<String, ModuleInputRequest>();
		return ir;
	}

	@Override
	public String getModuleName() {
		return "Perlin worm";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	private ArrayList<BlockPosition> generateWorm(double x, double y, double z, double lenght, double noiseScale) {
		ArrayList<BlockPosition> worm = new ArrayList<BlockPosition>();
		float movedLenght = 0.0f;
		
		worm.add(new BlockPosition(x, y, z));
		
		while(movedLenght < lenght) {
			float xDiff = (float) perlinGenerators[0].perlin3d(x/noiseScale, y/noiseScale, z/noiseScale);
			xDiff += (float) perlinGenerators[0].perlin3d(x/(0.25*noiseScale), y/(0.25*noiseScale), z/(0.25*noiseScale));
			float yDiff = (float) perlinGenerators[1].perlin3d(x/noiseScale, y/noiseScale, z/noiseScale);
			yDiff += (float) perlinGenerators[1].perlin3d(x/(0.25*noiseScale), y/(0.25*noiseScale), z/(0.25*noiseScale));
			float zDiff = (float) perlinGenerators[2].perlin3d(x/noiseScale, y/noiseScale, z/noiseScale);
			zDiff += (float) perlinGenerators[1].perlin3d(x/(0.25*noiseScale), y/(0.25*noiseScale), z/(0.25*noiseScale));
			
			if(xDiff == 0.0) xDiff += 0.1;
			if(yDiff == 0.0) yDiff += 0.1;
			if(zDiff == 0.0) zDiff += 0.1;
			float diff = (float) Math.sqrt(xDiff*xDiff + yDiff*yDiff + zDiff*zDiff);
			xDiff /= diff/1;
			yDiff /= diff/1;
			zDiff /= diff/1;
			
			x += xDiff;
			y += yDiff;
			z += zDiff;
			movedLenght += 1.0;
			
			worm.add(new BlockPosition(x, y, z));
		}
		
		return worm;
	}
	
	private void digWorm(ArrayList<BlockPosition> worm, DatatypeBlockspace blockspace, double radius, MaterialState<?, ?> material) {
		double xMin = blockspace.x - radius;
		double yMin = blockspace.y - radius;
		double zMin = blockspace.z - radius;
		double xMax = blockspace.x + blockspace.width + radius;
		double yMax = blockspace.y + blockspace.height + radius;
		double zMax = blockspace.z + blockspace.length + radius;
		double res = blockspace.resolution;
		
		for(BlockPosition bp: worm) {
			
			if(bp.x < xMin || bp.x > xMax) {
				continue;
			}
			else if(bp.y < yMin || bp.y > yMax) {
				continue;
			}
			else if(bp.z < zMin || bp.z > zMax) {
				continue;
			}
			
			for(double u = -radius/res; u < radius/res; u += res) {
				for(double v = -radius/res; v < radius/res; v += res) {
					for(double w = -radius/res; w < radius/res; w += res) {
						if(Math.sqrt(u*u+v*v+w*w) > radius) {
							continue;
						}
						setBlock((double)bp.x+u, (double)bp.y+v, (double)bp.z+w, material, blockspace);
					}
				}
			}
			
		}
	}
	
	private void setBlock(double x, double y, double z, MaterialState<?, ?> material, DatatypeBlockspace blockspace) {
		if(x < blockspace.x || x >= blockspace.x+blockspace.width) {
			return;
		}
		else if(y < blockspace.y || y >= blockspace.y+blockspace.height) {
			return;
		}
		else if(z < blockspace.z || z >= blockspace.z+blockspace.length) {
			return;
		}
		
		int ix = (int) Math.floor((x - blockspace.x) / blockspace.resolution);
		int iy = (int) Math.floor((y - blockspace.y) / blockspace.resolution);
		int iz = (int) Math.floor((z - blockspace.z) / blockspace.resolution);
		
		blockspace.getBlockspace()[ix][iy][iz] = material;
	}
	
	private ArrayList<BlockPosition> getStartingPoints(DatatypeBlockspace blockspace, double wormLenght, double distribution, double distributionRandom, long seed) {
		double x = blockspace.x - wormLenght;
		double y = blockspace.y - wormLenght;
		double z = blockspace.z - wormLenght;
		double width = blockspace.width + wormLenght*2;
		double height = blockspace.height + wormLenght*2;
		double lenght = blockspace.length + wormLenght*2;
		
		int minIndexX = (int) Math.floor(x / distribution);
		int maxIndexX = (int) Math.floor((x + width) / distribution);
		int minIndexY = (int) Math.floor(y / distribution);
		int maxIndexY = (int) Math.floor((y + height) / distribution);
		int minIndexZ = (int) Math.floor(z / distribution);
		int maxIndexZ = (int) Math.floor((z + lenght) / distribution);
		
		Permutation permutation = new Permutation(seed, 245, 3);
		
		ArrayList<BlockPosition> startingPoints = new ArrayList<BlockPosition>();
		
		for(int u = minIndexX; u <= maxIndexX; u++) {
			for(int v = minIndexY; v <= maxIndexY; v++) {
				for(int w = minIndexZ; w <= maxIndexZ; w++) {
					startingPoints.add(new BlockPosition(
							(u + (permutation.gUnitHash(0, u, v, w) - 0.5) * distributionRandom) * distribution,
							(v + (permutation.gUnitHash(1, u, v, w) - 0.5) * distributionRandom) * distribution,
							(w + (permutation.gUnitHash(2, u, v, w) - 0.5) * distributionRandom) * distribution));
				}
			}
		}
		
		return startingPoints;
	}
	
	private class PerlinGenerator {
		private int permutationSize = 256;
		private int repeat = permutationSize;
		
		/**
		 * This contains a double duplicated permutation table
		 */
		private int[] dp = new int[permutationSize*2];
		
		public PerlinGenerator(long seed) {
			int[] p = createPermutatationTable(permutationSize, seed);
			for(int pi = 0; pi < dp.length; pi++) {
				dp[pi] = p[pi%permutationSize];
			}
		}
		
		private int[] createPermutatationTable(int size, long seed) {
			//Create a random generator with supplied seed
			Random r = new Random(seed);
			
			//Generate a list containing every integer from 0 inclusive to size exlusive
			ArrayList<Integer> valueTabel = new ArrayList<Integer>();
			for(int i = 0; i < size; i++) {
				valueTabel.add(i);
			}
			
			//create the permutation table
			int[] permutationTable = new int[size];
			
			//Insert the values from the valueTable into the permutation table in a random order
			int pi = 0;
			while(valueTabel.size() > 0) {
				int index = r.nextInt(valueTabel.size());
				permutationTable[pi] = valueTabel.get(index);
				valueTabel.remove(index);
				pi++;
			}
			
			return permutationTable;
		}
		
		public double perlin3d(double x, double y, double z) {
			
			if(repeat > 0) {
				if(x < 0) {
					x = repeat+(x%repeat);
				}
				else {
					x = x%repeat;
				}
				if(y < 0) {
					y = repeat+(y%repeat);
				}
				else {
					y = y%repeat;
				}
				if(z < 0) {
					z = repeat+(z%repeat);
				}
				else {
					z = z%repeat;
				}
			}
			
			//Calculate the coordinates for the unit square that the coordinates is inside
			int xi = (int)x & 255;
			int yi = (int)y & 255;
			int zi = (int)z & 255;
			
			//Calculate the local coordinates inside the unit square
			double xf = x - (int)x;
			double yf = y - (int)y;
			double zf = z - (int)z;
			
			double u = easeCurve(xf);
			double v = easeCurve(yf);
			double w = easeCurve(zf);
			
			// zxy _ a=0, b=1
			int aaa, aab, aba, abb, baa, bab, bba, bbb;
			aaa = dp[dp[dp[xi     ]+yi     ]+zi     ];
			baa = dp[dp[dp[inc(xi)]+yi     ]+zi     ];
			bba = dp[dp[dp[inc(xi)]+inc(yi)]+zi     ];
			aba = dp[dp[dp[xi     ]+inc(yi)]+zi     ];
			aab = dp[dp[dp[xi     ]+yi     ]+inc(zi)];
			bab = dp[dp[dp[inc(xi)]+yi     ]+inc(zi)];
			bbb = dp[dp[dp[inc(xi)]+inc(yi)]+inc(zi)];
			abb = dp[dp[dp[xi     ]+inc(yi)]+inc(zi)];
			
			double a1, a2, a3, a4, a5, a6, a7, a8;
			a1 = grad(aaa, xf  , yf  , zf);
			a2 = grad(baa, xf-1, yf, zf);
			a3 = grad(aba, xf, yf-1, zf);
			a4 = grad(bba, xf-1, yf-1, zf);
			a5 = grad(aab, xf  , yf  , zf-1);
			a6 = grad(bab, xf-1, yf, zf-1);
			a7 = grad(abb, xf, yf-1, zf-1);
			a8 = grad(bbb, xf-1, yf-1, zf-1);
			
			double a12, a34, a56, a78, a1234, a5678;
			a12 = lerp(a1, a2, u);
			a34 = lerp(a3, a4, u);
			a56 = lerp(a5, a6, u);
			a78 = lerp(a7, a8, u);
			
			a1234 = lerp(a12, a34, v);
			a5678 = lerp(a56, a78, v);
			
			double value = lerp(a1234, a5678, w);
			return value;
		}
		
		private int inc(int num) {
			num++;
			int ret;
			if(num >= 0) ret = num % repeat;
			else ret = (repeat-1)+((num+1)%repeat);
			return ret;
		}
		
		private double easeCurve(double t) {
			return t * t * t * (t * (t * 6 - 15) + 10);
		}
		
		private double grad(int hash, double x, double y, double z) {
			switch(hash & 0xF)
		    {
			    case 0x0: return  x + y;
		        case 0x1: return -x + y;
		        case 0x2: return  x - y;
		        case 0x3: return -x - y;
		        case 0x4: return  x + z;
		        case 0x5: return -x + z;
		        case 0x6: return  x - z;
		        case 0x7: return -x - z;
		        case 0x8: return  y + z;
		        case 0x9: return -y + z;
		        case 0xA: return  y - z;
		        case 0xB: return -y - z;
		        case 0xC: return  y + x;
		        case 0xD: return -y + z;
		        case 0xE: return  y - x;
		        case 0xF: return -y - z;
		        default: return 0; // never happens
		    }
		}
		
		private double lerp(double a, double b, double x) {
			return a + x * (b - a);
		}
	}
	
	private class BlockPosition {
		public double x, y, z;
		
		public BlockPosition(double x, double y, double z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
}
