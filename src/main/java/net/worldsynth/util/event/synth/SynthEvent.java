/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event.synth;

import java.util.EventObject;

import net.worldsynth.modulewrapper.ModuleWrapper;

public class SynthEvent extends EventObject {
	private static final long serialVersionUID = 7606299207519621064L;
	
	private SynthEventType eventType;
	private ModuleWrapper device;
	
	public SynthEvent(SynthEventType eventType, ModuleWrapper device, Object source) {
		super(source);
		this.eventType = eventType;
		this.device = device;
	}
	
	public ModuleWrapper getDevice() {
		return device;
	}
	
	public SynthEventType getEventType() {
		return eventType;
	}
	
	public enum SynthEventType {
		MODULE_ADDED,
		MODULE_REMOVES,
		MODULE_MODIFIED;
	}
}
