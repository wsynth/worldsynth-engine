/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event.module;

import javafx.event.Event;

public class ModuleBypassChangeEvent extends Event {
	private static final long serialVersionUID = -7955269316224933178L;
	
	private final boolean oldBypassValue;
	private final boolean newBypassValue;
	
	public ModuleBypassChangeEvent(boolean oldBypassValue, boolean newBypassValue) {
		super(null);
		this.oldBypassValue = oldBypassValue;
		this.newBypassValue = newBypassValue;
	}
	
	public boolean getOldBypassValue() {
		return oldBypassValue;
	}
	
	public boolean getNewBypassValue() {
		return newBypassValue;
	}
}
