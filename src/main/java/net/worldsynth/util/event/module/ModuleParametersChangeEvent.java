/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event.module;

import javafx.event.Event;
import net.worldsynth.synth.io.Element;

public class ModuleParametersChangeEvent extends Event {
	private static final long serialVersionUID = -7955269316224933178L;
	
	private final Element oldParametersElement;
	private final Element newParametersElement;
	
	public ModuleParametersChangeEvent(Element oldParametersElement, Element newParametersElement) {
		super(null);
		this.oldParametersElement = oldParametersElement;
		this.newParametersElement = newParametersElement;
	}
	
	public Element getOldParametersElement() {
		return oldParametersElement;
	}
	
	public Element getNewParametersElement() {
		return newParametersElement;
	} 
	
}
