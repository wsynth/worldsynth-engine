/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event.module;

import javafx.event.Event;

public class ModuleApplyParametersEvent extends Event {
	private static final long serialVersionUID = -6577128138770823364L;

	public ModuleApplyParametersEvent() {
		super(null);
	}
}
