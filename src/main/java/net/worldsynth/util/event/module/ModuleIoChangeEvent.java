/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event.module;

import javafx.event.Event;

public class ModuleIoChangeEvent extends Event {
	private static final long serialVersionUID = 3477520059797146016L;

	public ModuleIoChangeEvent() {
		super(null);
	}
}
