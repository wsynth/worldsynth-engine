/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event.build;

import java.util.EventObject;

import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.util.event.build.BuildStatus.BuildState;

public class BuildStatusEvent extends EventObject {
	private static final long serialVersionUID = 5609339495579651489L;
	
	private final BuildStatus status;
	
	public BuildStatusEvent(BuildStatus status, Object source) {
		this(status.getBuildState(), status.getMessage(), status.getDevice(), status.getRequest(), status.getBuildThread(), source);
	}
	
	public BuildStatusEvent(BuildState buildState, String message, ModuleWrapper device, ModuleOutputRequest request, Thread buildThread, Object source) {
		super(source);
		status = new BuildStatus(buildState, message, device, request, buildThread);
	}
	
	public BuildStatus getStatus() {
		return status;
	}
}
