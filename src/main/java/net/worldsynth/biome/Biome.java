/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.biome;

import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javafx.scene.paint.Color;
import net.worldsynth.common.color.WsColor;

@JsonDeserialize(using = BiomeJsonDeserializer.class)
public class Biome implements Comparable<Biome> {
	
	public static Biome NULL = new Biome("", "", new WsColor(0, 0, 0), null);
	
	protected String idName;
	protected String displayName;
	protected  WsColor color;
	
	protected Set<String> tags; //Optional tag list for extra filtering
	
	public Biome(String idName, String displayName, WsColor color, Set<String> tags) {
		this.idName = idName;
		this.displayName = displayName;
		this.color = color;
	}
	
	public String getIdName() {
		return idName;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public WsColor getWsColor() {
		return color;
	}
	
	public Color getFxColor() {
		return color.getFxColor();
	}
	
	@Override
	public String toString() {
		return getDisplayName();
	}

	@Override
	public int compareTo(Biome comp) {
		return getDisplayName().compareTo(comp.getDisplayName());
	}
}
