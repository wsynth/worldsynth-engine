/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.extent;

import javafx.beans.NamedArg;
import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

public class ExtentEvent extends Event {

	private static final long serialVersionUID = -6685700515592965314L;
	
	private final WorldExtent extent;
	
	/**
	 * Common supertype for all extent event types.
	 */
	public static final EventType<ExtentEvent> ANY = new EventType<ExtentEvent>(Event.ANY, "EXTENT");

	/**
	 * This event occurs when an extent has been added to the extentmanager.
	 */
	public static final EventType<ExtentEvent> EXTENT_ADDED = new EventType<ExtentEvent>(ExtentEvent.ANY, "EXTENT_ADDED");
	
	/**
	 * NOT IMPLEMENTED<br><br>
	 * This event occurs when an extent has been altered.
	 */
	public static final EventType<ExtentEvent> EXTENT_EDITED = new EventType<ExtentEvent>(ExtentEvent.ANY, "EXTENT_EDITED");

	/**
	 * This event occurs when an extent has been removed from the extentmanager.
	 */
	public static final EventType<ExtentEvent> EXTENT_REMOVED = new EventType<ExtentEvent>(ExtentEvent.ANY, "EXTENT_REMOVED");
	
	
	/**
     * Creates new instance of ExtentEvent.
     * @param eventType Type of the event
     * @param extent    Extent the event is relevant for
     */
    public ExtentEvent(final @NamedArg("eventType") EventType<? extends ExtentEvent> eventType, final @NamedArg("extent") WorldExtent extent) {
        super(eventType);
        this.extent = extent;
    }
	
	/**
	 * Constructs new ExtentEvent event with null source and target and KeyCode
	 * object directly specified.
	 * 
	 * @param source    the source of the event. Can be null.
	 * @param target    the target of the event. Can be null.
	 * @param eventType The type of the event.
	 * @param extent    Extent the event is relevant for
	 */
	public ExtentEvent(@NamedArg("source") Object source, @NamedArg("target") EventTarget target, @NamedArg("eventType") EventType<ExtentEvent> eventType, final @NamedArg("extent") WorldExtent extent) {
		super(source, target, eventType);
		this.extent = extent;
	}

	@Override
	public EventType<? extends ExtentEvent> getEventType() {
		return (EventType<? extends ExtentEvent>) super.getEventType();
	}
    
    public WorldExtent getExtent() {
    	return extent;
    }
}
