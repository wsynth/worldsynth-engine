/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon;

import java.util.List;

import net.worldsynth.biome.BiomeProfile;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.material.MaterialProfile;
import net.worldsynth.module.AbstractModuleRegister;

public interface IAddonLoader {
	
	public List<AbstractModuleRegister> getAddonModuleRegisters();
	
	@SuppressWarnings("rawtypes")
	public List<MaterialProfile> getAddonMaterialProfiles();
	
	@SuppressWarnings("rawtypes")
	public List<BiomeProfile> getAddonBiomeProfiles();
	
	public List<CustomObjectFormat> getAddonCustomObjectFormats();
}
