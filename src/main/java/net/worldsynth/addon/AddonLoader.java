/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.worldsynth.biome.BiomeProfile;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.material.MaterialProfile;
import net.worldsynth.module.AbstractModuleRegister;

public class AddonLoader implements IAddonLoader {
	private static final Logger logger = LogManager.getLogger(AddonLoader.class);
	
	private final ArrayList<AddonJarLoader> jarLoaders = new ArrayList<AddonJarLoader>();
	private final ArrayList<IAddonLoader> injectedAddonsLoaders = new ArrayList<IAddonLoader>();
	
	public AddonLoader(File addonDirectory, IAddonLoader... injectedAddonLoaders) {
		if(injectedAddonLoaders != null) {
			this.injectedAddonsLoaders.addAll(Arrays.asList(injectedAddonLoaders));
		}
		loadAddonsFromDirectory(addonDirectory);
	}
	
	private void loadAddonsFromDirectory(File addonDirectory) {
		if(!addonDirectory.isDirectory()) {
			logger.error("Addon directory \"" + addonDirectory.getAbsolutePath() + "\" does not exist");
			return;
		}
		
		for(File sub: addonDirectory.listFiles()) {
			if(sub.isDirectory()) {
				loadAddonsFromDirectory(sub);
			}
			else if(sub.getAbsolutePath().endsWith(".jar")) {
				try {
					loadAddon(sub);
				} catch(ClassNotFoundException | IOException e) {
					logger.error("Problem loading addon: " + sub.getName(), e);
				}
			}
		}
	}
	
	private void loadAddon(File jar) throws ClassNotFoundException, IOException {
		AddonJarLoader jarLoader = new AddonJarLoader(jar);
		jarLoaders.add(jarLoader);
	}
	
	private <T> List<T> getInstancesAssignableFrom(Class<T> classType) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<T> instances = new ArrayList<T>();
		for(AddonJarLoader jarLoader: jarLoaders) {
			instances.addAll(jarLoader.getInstancesAssignableFrom(classType));
		}
		return instances;
	}
	
	@Override
	public List<AbstractModuleRegister> getAddonModuleRegisters() {
		ArrayList<AbstractModuleRegister> moduleRegisters = new ArrayList<AbstractModuleRegister>();
		for(IAddonLoader injectedAddon: injectedAddonsLoaders) {
			moduleRegisters.addAll(injectedAddon.getAddonModuleRegisters());
		}
		try {
			moduleRegisters.addAll(getInstancesAssignableFrom(AbstractModuleRegister.class));
		} catch(NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			logger.error("Problem during instancing of addon module registers", e);
		}
		return moduleRegisters;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<MaterialProfile> getAddonMaterialProfiles() {
		ArrayList<MaterialProfile> materialProfiles = new ArrayList<MaterialProfile>();
		for(IAddonLoader injectedAddon: injectedAddonsLoaders) {
			materialProfiles.addAll(injectedAddon.getAddonMaterialProfiles());
		}
		try {
			materialProfiles.addAll(getInstancesAssignableFrom(MaterialProfile.class));
		} catch(NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			logger.error("Problem during instancing of addon material profiles", e);
		}
		return materialProfiles;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<BiomeProfile> getAddonBiomeProfiles() {
		ArrayList<BiomeProfile> biomeProfiles = new ArrayList<BiomeProfile>();
		for(IAddonLoader injectedAddon: injectedAddonsLoaders) {
			biomeProfiles.addAll(injectedAddon.getAddonBiomeProfiles());
		}
		try {
			biomeProfiles.addAll(getInstancesAssignableFrom(BiomeProfile.class));
		} catch(NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			logger.error("Problem during instancing of addon biome profiles", e);
		}
		return biomeProfiles;
	}
	
	@Override
	public List<CustomObjectFormat> getAddonCustomObjectFormats() {
		ArrayList<CustomObjectFormat> objectFormats = new ArrayList<CustomObjectFormat>();
		for(IAddonLoader injectedAddon: injectedAddonsLoaders) {
			objectFormats.addAll(injectedAddon.getAddonCustomObjectFormats());
		}
		try {
			objectFormats.addAll(getInstancesAssignableFrom(CustomObjectFormat.class));
		} catch(NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			logger.error("Problem during instancing of addon custom object formats", e);
		}
		return objectFormats;
	}
}
