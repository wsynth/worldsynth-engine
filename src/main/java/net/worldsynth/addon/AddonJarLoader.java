/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import net.worldsynth.module.AbstractModuleRegister;

public class AddonJarLoader {

	private ArrayList<Class<?>> loadedClasses = new ArrayList<Class<?>>();

	public AddonJarLoader(File jar) throws IOException, ClassNotFoundException {
		String pathToJar = jar.getAbsolutePath();
		JarFile jarFile = new JarFile(pathToJar);
		Enumeration<JarEntry> allJarEntries = jarFile.entries();

		// Load all classes in the jar
		URL[] urls = { new URL("jar:file:" + pathToJar + "!/") };
		ClassLoader cl = URLClassLoader.newInstance(urls, AbstractModuleRegister.class.getClassLoader());
		while(allJarEntries.hasMoreElements()) {
			JarEntry jarEntry = allJarEntries.nextElement();
			if(jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class")) {
				continue;
			}
			String className = jarEntry.getName().substring(0, jarEntry.getName().length() - 6);
			className = className.replace('/', '.');
			loadedClasses.add(cl.loadClass(className));
		}

		jarFile.close();
	}

	public <T> List<T> getInstancesAssignableFrom(Class<T> classType) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<T> instances = new ArrayList<T>();
		for(Class<?> c: loadedClasses) {
			if(classType.isAssignableFrom(c)) {
				@SuppressWarnings("unchecked")
				Class<T> assignableClass = (Class<T>) c;
				Constructor<T> classConstructor = assignableClass.getConstructor();
				instances.add(classConstructor.newInstance());
			}
		}
		return instances;
	}
}
