# WorldSynth Engine
The WorldSynth Engine repository has been merged with the WorldSynth Patcher repository, and development in this repository is discontinued.  
Please see the new combined repository at: https://gitlab.com/wsynth/worldsynth
